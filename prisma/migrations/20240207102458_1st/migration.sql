-- CreateTable
CREATE TABLE `gtent` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `compcode` VARCHAR(30) NOT NULL,
    `compname` VARCHAR(200) NOT NULL,
    `owner` VARCHAR(200) NOT NULL,
    `brand` VARCHAR(200) NOT NULL,
    `othindustry` VARCHAR(100) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `phone` VARCHAR(20) NOT NULL,
    `addr1` VARCHAR(200) NOT NULL,
    `addr2` VARCHAR(200) NULL,
    `city` VARCHAR(100) NOT NULL,
    `itemcodelength` INTEGER NULL,
    `batchlength` INTEGER NULL,
    `synccrm` INTEGER NULL,
    `maxlevelhierarchy` INTEGER NULL,
    `syncgoodstracker` INTEGER NULL,
    `baseurlcrm` TEXT NULL,
    `baseurlinterfacing` TEXT NULL,

    UNIQUE INDEX `gtent_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tcurr` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `curcode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `descriptionfrgn` VARCHAR(100) NOT NULL,

    UNIQUE INDEX `tcurr_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tocry` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `countrycode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(30) NOT NULL,
    `descriptionfrgn` VARCHAR(30) NOT NULL,
    `phonecode` VARCHAR(5) NOT NULL,
    `tcurrId` BIGINT NULL,

    UNIQUE INDEX `tocry_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toprv` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `provnccode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `descriptionfrgn` VARCHAR(100) NOT NULL,
    `tocryId` BIGINT NULL,

    UNIQUE INDEX `toprv_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tozcd` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `zipcode` VARCHAR(30) NOT NULL,
    `city` VARCHAR(100) NOT NULL,
    `district` VARCHAR(100) NOT NULL,
    `urban` VARCHAR(100) NULL,
    `subdistrict` VARCHAR(100) NOT NULL,
    `toprvId` BIGINT NULL,

    UNIQUE INDEX `tozcd_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tostr` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `storecode` VARCHAR(30) NOT NULL,
    `storename` VARCHAR(200) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `phone` VARCHAR(20) NOT NULL,
    `addr1` VARCHAR(200) NOT NULL,
    `addr2` VARCHAR(200) NULL,
    `addr3` VARCHAR(200) NULL,
    `city` VARCHAR(100) NOT NULL,
    `remarks` TEXT NULL,
    `toprvId` BIGINT NULL,
    `tocryId` BIGINT NULL,
    `tozcdId` BIGINT NULL,
    `tohemId` BIGINT NULL,
    `sqm` DOUBLE NOT NULL,
    `tcurrId` BIGINT NULL,
    `toplnId` BIGINT NULL,
    `storepic` BLOB NULL,
    `tovatId` BIGINT NULL,
    `storeopen` DATE NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,
    `prefixdoc` VARCHAR(30) NULL DEFAULT '',
    `header01` VARCHAR(48) NULL DEFAULT '-',
    `header02` VARCHAR(48) NULL DEFAULT '-',
    `header03` VARCHAR(48) NULL DEFAULT '-',
    `header04` VARCHAR(48) NULL DEFAULT '-',
    `header05` VARCHAR(48) NULL DEFAULT '-',
    `footer01` VARCHAR(48) NULL DEFAULT '-',
    `footer02` VARCHAR(48) NULL DEFAULT '-',
    `footer03` VARCHAR(48) NULL DEFAULT '-',
    `footer04` VARCHAR(48) NULL DEFAULT '-',
    `footer05` VARCHAR(48) NULL DEFAULT '-',
    `sellingtax` DOUBLE NULL DEFAULT 0.0,
    `openingbalance` DOUBLE NULL DEFAULT 0.0,
    `credittaxcodeId` BIGINT NULL,
    `tpmt1Id` BIGINT NULL,

    UNIQUE INDEX `tostr_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tohem` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `empcode` VARCHAR(30) NOT NULL,
    `empname` VARCHAR(200) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `phone` VARCHAR(20) NOT NULL,
    `addr1` VARCHAR(200) NOT NULL,
    `addr2` VARCHAR(200) NULL,
    `addr3` VARCHAR(200) NULL,
    `city` VARCHAR(100) NOT NULL,
    `remarks` TEXT NULL,
    `toprvId` BIGINT NULL,
    `tocryId` BIGINT NULL,
    `tozcdId` BIGINT NULL,
    `idcard` VARCHAR(30) NOT NULL,
    `gender` VARCHAR(1) NOT NULL,
    `birthdate` DATE NOT NULL,
    `photo` BLOB NULL,
    `joindate` DATE NOT NULL,
    `resigndate` DATE NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,
    `empdept` VARCHAR(200) NOT NULL,
    `emptitle` VARCHAR(200) NOT NULL,
    `empworkplace` VARCHAR(200) NOT NULL,
    `empdebt` DOUBLE NOT NULL,

    UNIQUE INDEX `tohem_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tocsr` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `hwkey` VARCHAR(100) NOT NULL,
    `token` VARCHAR(100) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `ipkassa` VARCHAR(15) NULL,
    `idkassa` VARCHAR(50) NULL,
    `printercode` VARCHAR(20) NULL,
    `printlogo` INTEGER NULL,
    `struktype` INTEGER NULL,
    `bigheader` INTEGER NULL,
    `synccloud` INTEGER NULL,

    UNIQUE INDEX `tocsr_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tcsr1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tocsrId` BIGINT NULL,
    `tousrId` BIGINT NULL,
    `docnum` VARCHAR(30) NOT NULL,
    `opendate` DATE NOT NULL,
    `opentime` TIME(0) NOT NULL,
    `calcdate` DATE NOT NULL,
    `calctime` TIME(0) NOT NULL,
    `closedate` DATE NOT NULL,
    `closetime` TIME(0) NOT NULL,
    `timezone` VARCHAR(200) NOT NULL,
    `openvalue` DOUBLE NOT NULL,
    `calcvalue` DOUBLE NOT NULL,
    `cashvalue` DOUBLE NOT NULL,
    `closevalue` DOUBLE NOT NULL,
    `openedbyId` BIGINT NULL,
    `closedbyId` BIGINT NULL,

    UNIQUE INDEX `tcsr1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tousr` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `email` VARCHAR(100) NOT NULL,
    `username` VARCHAR(100) NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    `tohemId` BIGINT NULL,
    `torolId` BIGINT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,
    `superuser` INTEGER NOT NULL,
    `provider` INTEGER NOT NULL,
    `usertype` INTEGER NULL,
    `trolleyuser` VARCHAR(20) NULL,
    `trolleypass` VARCHAR(100) NULL,
    `tostrId` BIGINT NULL,

    UNIQUE INDEX `tousr_docid_key`(`docid`),
    UNIQUE INDEX `tousr_email_key`(`email`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `torol` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `rolecode` VARCHAR(30) NOT NULL,
    `rolename` VARCHAR(100) NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,

    UNIQUE INDEX `torol_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toaut` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tformId` BIGINT NULL,
    `tousrId` BIGINT NULL,
    `authorization` INTEGER NOT NULL,
    `setby` INTEGER NOT NULL,

    UNIQUE INDEX `toaut_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tocat` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `catcode` VARCHAR(30) NOT NULL,
    `catname` VARCHAR(100) NOT NULL,
    `catnamefrgn` VARCHAR(100) NOT NULL,
    `parentId` BIGINT NULL,
    `level` INTEGER NOT NULL,
    `phir1Id` BIGINT NULL,
    `synccrm` INTEGER NOT NULL DEFAULT 0,

    UNIQUE INDEX `tocat_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `touom` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `uomcode` VARCHAR(30) NOT NULL,
    `uomdesc` VARCHAR(100) NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,

    UNIQUE INDEX `touom_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toitm` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `itemcode` VARCHAR(30) NOT NULL,
    `itemname` VARCHAR(100) NOT NULL,
    `invitem` INTEGER NOT NULL,
    `serialno` INTEGER NOT NULL,
    `tocatId` BIGINT NULL,
    `touomId` BIGINT NULL,
    `minstock` DOUBLE NOT NULL,
    `maxstock` DOUBLE NOT NULL,
    `includetax` INTEGER NOT NULL,
    `remarks` TEXT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,
    `isbatch` INTEGER NOT NULL DEFAULT 0,
    `sync` INTEGER NOT NULL DEFAULT 0,
    `internalcode_1` VARCHAR(100) NULL DEFAULT '',
    `internalcode_2` VARCHAR(100) NULL DEFAULT '',
    `property1` BIGINT NULL,
    `property2` BIGINT NULL,
    `property3` BIGINT NULL,
    `property4` BIGINT NULL,
    `property5` BIGINT NULL,
    `property6` BIGINT NULL,
    `property7` BIGINT NULL,
    `property8` BIGINT NULL,
    `property9` BIGINT NULL,
    `property10` BIGINT NULL,
    `openprice` INTEGER NOT NULL DEFAULT 0,

    UNIQUE INDEX `toitm_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tbitm` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `barcode` VARCHAR(50) NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,
    `quantity` DOUBLE NOT NULL,
    `touomId` BIGINT NULL,

    UNIQUE INDEX `tbitm_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tsitm` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,
    `tovatId` BIGINT NULL,
    `tovatIdPur` BIGINT NULL,

    UNIQUE INDEX `tsitm_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tvitm` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tsitmId` BIGINT NULL,
    `listing` INTEGER NOT NULL,
    `minorder` DOUBLE NOT NULL,
    `multipyorder` DOUBLE NOT NULL,
    `canorder` INTEGER NOT NULL,
    `dflt` INTEGER NOT NULL,

    UNIQUE INDEX `tvitm_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tpitm` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `picture` BLOB NOT NULL,

    UNIQUE INDEX `tpitm_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tritm` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `remarks` TEXT NULL,

    UNIQUE INDEX `tritm_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `topln` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `pricecode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `baseprice` BIGINT NOT NULL,
    `periodprice` BIGINT NOT NULL,
    `factor` DOUBLE NOT NULL,
    `tcurrId` BIGINT NULL,
    `type` INTEGER NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,

    UNIQUE INDEX `topln_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tpln1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toplnId` BIGINT NULL,
    `periodfr` DATE NOT NULL,
    `periodto` DATE NOT NULL,
    `baseprice` BIGINT NOT NULL,
    `periodprice` BIGINT NOT NULL,
    `factor` DOUBLE NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,

    UNIQUE INDEX `tpln1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tpln2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tpln1Id` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `tcurrId` BIGINT NULL,
    `price` DOUBLE NOT NULL,

    UNIQUE INDEX `tpln2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tpln3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toplnId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,

    UNIQUE INDEX `tpln3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tpln4` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tpln2Id` BIGINT NULL,
    `tbitmId` BIGINT NULL,
    `price` DOUBLE NOT NULL,

    UNIQUE INDEX `tpln4_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tobpt` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `basecode` VARCHAR(1) NOT NULL,
    `description` VARCHAR(100) NOT NULL,

    UNIQUE INDEX `tobpt_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `togen` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `gendercode` VARCHAR(1) NOT NULL,
    `description` VARCHAR(100) NOT NULL,

    UNIQUE INDEX `togen_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tocrg` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `custgroupcode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `maxdiscount` DOUBLE NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,

    UNIQUE INDEX `tocrg_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tocus` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `custcode` VARCHAR(191) NOT NULL,
    `custname` VARCHAR(100) NOT NULL,
    `tocrgId` BIGINT NULL,
    `idcard` VARCHAR(30) NOT NULL,
    `taxno` VARCHAR(50) NOT NULL,
    `gender` VARCHAR(1) NOT NULL,
    `birthdate` DATE NOT NULL,
    `addr1` VARCHAR(200) NOT NULL,
    `addr2` VARCHAR(200) NULL,
    `addr3` VARCHAR(200) NULL,
    `city` VARCHAR(100) NOT NULL,
    `toprvId` BIGINT NULL,
    `tocryId` BIGINT NULL,
    `tozcdId` BIGINT NULL,
    `phone` VARCHAR(20) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `remarks` TEXT NULL,
    `toptrId` BIGINT NULL,
    `toplnId` BIGINT NULL,
    `joindate` DATE NULL,
    `maxdiscount` DOUBLE NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,
    `isemployee` INTEGER NOT NULL DEFAULT 0,
    `tohemId` BIGINT NULL,

    UNIQUE INDEX `tocus_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tcus1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tocusId` BIGINT NULL,
    `linenum` INTEGER NOT NULL,
    `addr1` VARCHAR(200) NOT NULL,
    `addr2` VARCHAR(200) NULL,
    `addr3` VARCHAR(200) NULL,
    `city` VARCHAR(100) NOT NULL,
    `toprvId` BIGINT NULL,
    `tocryId` BIGINT NULL,
    `tozcdId` BIGINT NULL,

    UNIQUE INDEX `tcus1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tcus2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tocusId` BIGINT NULL,
    `linenum` INTEGER NOT NULL,
    `title` VARCHAR(20) NOT NULL,
    `fullname` VARCHAR(200) NOT NULL,
    `phone` VARCHAR(20) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `position` VARCHAR(50) NOT NULL,
    `idcard` VARCHAR(30) NOT NULL,
    `taxno` VARCHAR(50) NOT NULL,
    `gender` VARCHAR(1) NOT NULL,
    `birthdate` DATE NOT NULL,

    UNIQUE INDEX `tcus2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toptr` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `paymentcode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `base` VARCHAR(1) NOT NULL,
    `dueon` INTEGER NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,

    UNIQUE INDEX `toptr_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `gopmt` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `paytypecode` VARCHAR(10) NOT NULL,
    `description` VARCHAR(100) NOT NULL,

    UNIQUE INDEX `gopmt_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `topmt` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `paytypecode` VARCHAR(10) NOT NULL,
    `description` VARCHAR(100) NOT NULL,

    UNIQUE INDEX `topmt_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tpmt1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `topmtId` BIGINT NULL,
    `mopcode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `mopalias` VARCHAR(100) NOT NULL,
    `bankcharge` DOUBLE NOT NULL,
    `consolidation` INTEGER NOT NULL,
    `credit` INTEGER NOT NULL,
    `subtype` INTEGER NOT NULL DEFAULT 0,
    `validforemp` INTEGER NOT NULL DEFAULT 0,

    UNIQUE INDEX `tpmt1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tpmt2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `cccode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `cardtype` INTEGER NOT NULL DEFAULT 0,
    `statusactive` INTEGER NOT NULL DEFAULT 0,
    `activated` INTEGER NOT NULL DEFAULT 0,

    UNIQUE INDEX `tpmt2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tpmt3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tpmt1Id` BIGINT NULL,
    `tostrId` BIGINT NULL,

    UNIQUE INDEX `tpmt3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tobnk` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `accountno` VARCHAR(30) NOT NULL,
    `accountname` VARCHAR(200) NOT NULL,
    `bank` VARCHAR(100) NOT NULL,
    `tostrId` BIGINT NULL,

    UNIQUE INDEX `tobnk_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toitt` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `quantity` DOUBLE NOT NULL,
    `touomId` BIGINT NULL,
    `tipe` INTEGER NOT NULL,

    UNIQUE INDEX `toitt_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `titt1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toittId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `quantity` DOUBLE NOT NULL,
    `touomId` BIGINT NULL,

    UNIQUE INDEX `titt1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tovat` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `taxcode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `rate` DOUBLE NOT NULL,
    `periodfrom` DATE NOT NULL,
    `periodto` DATE NOT NULL,
    `taxtype` VARCHAR(1) NOT NULL,
    `statusactive` INTEGER NOT NULL,
    `activated` INTEGER NOT NULL,

    UNIQUE INDEX `tovat_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `taut1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tformId` BIGINT NULL,
    `torolId` BIGINT NULL,
    `authorization` INTEGER NOT NULL,
    `canview` INTEGER NULL,
    `cancreate` INTEGER NULL,
    `canupdate` INTEGER NULL,

    UNIQUE INDEX `taut1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tform` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `formcode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `descriptionfrgn` VARCHAR(100) NOT NULL,

    UNIQUE INDEX `tform_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tphir` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `description` VARCHAR(100) NOT NULL,
    `level` INTEGER NOT NULL,
    `maxchar` INTEGER NOT NULL,
    `synccrm` INTEGER NOT NULL DEFAULT 0,

    UNIQUE INDEX `tphir_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `phir1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `code` VARCHAR(30) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `tphirId` BIGINT NULL,
    `synccrm` INTEGER NOT NULL DEFAULT 0,

    UNIQUE INDEX `phir1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprop` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `properties` VARCHAR(30) NOT NULL,
    `code` VARCHAR(30) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `synccrm` INTEGER NOT NULL DEFAULT 0,

    UNIQUE INDEX `tprop_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toprp` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `promocode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `startdate` DATE NOT NULL,
    `enddate` DATE NOT NULL,
    `starttime` TIME(0) NOT NULL,
    `endtime` TIME(0) NOT NULL,
    `remarks` TEXT NULL,
    `statusactive` INTEGER NOT NULL,
    `toplnId` BIGINT NULL,

    UNIQUE INDEX `toprp_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprp1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprpId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `promotype` VARCHAR(1) NOT NULL,
    `minquantity` DOUBLE NOT NULL,
    `promovalue` DOUBLE NOT NULL,

    UNIQUE INDEX `tprp1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprp2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprpId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `holiday` INTEGER NOT NULL,
    `day1` INTEGER NOT NULL,
    `day2` INTEGER NOT NULL,
    `day3` INTEGER NOT NULL,
    `day4` INTEGER NOT NULL,
    `day5` INTEGER NOT NULL,
    `day6` INTEGER NOT NULL,
    `day7` INTEGER NOT NULL,

    UNIQUE INDEX `tprp2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprp3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tprp2Id` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprp3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprp4` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprpId` BIGINT NULL,
    `tocrgId` BIGINT NULL,

    UNIQUE INDEX `tprp4_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprp8` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprpId` BIGINT NULL,
    `promotype` VARCHAR(1) NOT NULL,
    `minquantity` DOUBLE NOT NULL,
    `promovalue` DOUBLE NOT NULL,

    UNIQUE INDEX `tprp8_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprp9` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprpId` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprp9_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toprc` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `promocode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `startdate` DATE NOT NULL,
    `enddate` DATE NOT NULL,
    `starttime` TIME(0) NOT NULL,
    `endtime` TIME(0) NOT NULL,
    `remarks` TEXT NULL,
    `minpurchase` DOUBLE NOT NULL,
    `discpct` DOUBLE NOT NULL,
    `discvalue` DOUBLE NOT NULL,
    `statusactive` INTEGER NOT NULL,

    UNIQUE INDEX `toprc_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprc1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprcId` BIGINT NULL,
    `tpmt2Id` BIGINT NULL,

    UNIQUE INDEX `tprc1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprc2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprcId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `holiday` INTEGER NOT NULL,
    `day1` INTEGER NOT NULL,
    `day2` INTEGER NOT NULL,
    `day3` INTEGER NOT NULL,
    `day4` INTEGER NOT NULL,
    `day5` INTEGER NOT NULL,
    `day6` INTEGER NOT NULL,
    `day7` INTEGER NOT NULL,

    UNIQUE INDEX `tprc2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprc3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tprc2Id` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprc3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprc4` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprcId` BIGINT NULL,
    `tocrgId` BIGINT NULL,

    UNIQUE INDEX `tprc4_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprc5` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprcId` BIGINT NULL,
    `tocatId` BIGINT NULL,

    UNIQUE INDEX `tprc5_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprc9` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprcId` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprc9_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toprb` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `promocode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `startdate` DATE NOT NULL,
    `enddate` DATE NOT NULL,
    `starttime` TIME(0) NOT NULL,
    `endtime` TIME(0) NOT NULL,
    `remarks` TEXT NULL,
    `minpurchase` DOUBLE NOT NULL,
    `buycondition` INTEGER NOT NULL,
    `minbuy` DOUBLE NOT NULL,
    `maxmultiply` DOUBLE NOT NULL,
    `getcondition` INTEGER NOT NULL,
    `maxget` DOUBLE NOT NULL,
    `statusactive` INTEGER NOT NULL,

    UNIQUE INDEX `toprb_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprb1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprbId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `quantity` DOUBLE NOT NULL,

    UNIQUE INDEX `tprb1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprb2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprbId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `holiday` INTEGER NOT NULL,
    `day1` INTEGER NOT NULL,
    `day2` INTEGER NOT NULL,
    `day3` INTEGER NOT NULL,
    `day4` INTEGER NOT NULL,
    `day5` INTEGER NOT NULL,
    `day6` INTEGER NOT NULL,
    `day7` INTEGER NOT NULL,

    UNIQUE INDEX `tprb2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprb3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tprb2Id` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprb3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprb4` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprbId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `quantity` DOUBLE NOT NULL,
    `sellingprice` DOUBLE NOT NULL,

    UNIQUE INDEX `tprb4_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprb5` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprbId` BIGINT NULL,
    `tocrgId` BIGINT NULL,

    UNIQUE INDEX `tprb5_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprb6` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprbId` BIGINT NULL,
    `tocatId` BIGINT NULL,
    `tocrgId` BIGINT NULL,

    UNIQUE INDEX `tprb6_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprb9` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprbId` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprb9_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toprk` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `promocode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `startdate` DATE NOT NULL,
    `enddate` DATE NOT NULL,
    `starttime` TIME(0) NOT NULL,
    `endtime` TIME(0) NOT NULL,
    `remarks` TEXT NULL,
    `discount` DOUBLE NOT NULL,
    `disctype` INTEGER NOT NULL,
    `globaldisc` VARCHAR(1) NOT NULL,
    `minquantity` DOUBLE NOT NULL,
    `statusactive` INTEGER NOT NULL,

    UNIQUE INDEX `toprk_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprk1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprkId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `minquantity` DOUBLE NOT NULL,

    UNIQUE INDEX `tprk1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprk2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprkId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `holiday` INTEGER NOT NULL,
    `day1` INTEGER NOT NULL,
    `day2` INTEGER NOT NULL,
    `day3` INTEGER NOT NULL,
    `day4` INTEGER NOT NULL,
    `day5` INTEGER NOT NULL,
    `day6` INTEGER NOT NULL,
    `day7` INTEGER NOT NULL,

    UNIQUE INDEX `tprk2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprk3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tprk2Id` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprk3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprk4` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprkId` BIGINT NULL,
    `tocrgId` BIGINT NULL,

    UNIQUE INDEX `tprk4_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprk5` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprkId` BIGINT NULL,
    `tocatId` BIGINT NULL,

    UNIQUE INDEX `tprk5_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprk9` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprkId` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprk9_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toprg` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `promocode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `startdate` DATE NOT NULL,
    `enddate` DATE NOT NULL,
    `starttime` TIME(0) NOT NULL,
    `endtime` TIME(0) NOT NULL,
    `remarks` TEXT NULL,
    `priority` INTEGER NOT NULL,
    `priorityno` INTEGER NOT NULL,
    `minpurchase` DOUBLE NOT NULL,
    `maxget` DOUBLE NOT NULL,
    `getcondition` VARCHAR(10) NOT NULL,
    `statusactive` INTEGER NOT NULL,

    UNIQUE INDEX `toprg_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprg1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprgId` BIGINT NULL,
    `toitmId` BIGINT NULL,
    `quantity` DOUBLE NOT NULL,
    `sellingprice` DOUBLE NOT NULL,

    UNIQUE INDEX `tprg1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprg2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprgId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `holiday` INTEGER NOT NULL,
    `day1` INTEGER NOT NULL,
    `day2` INTEGER NOT NULL,
    `day3` INTEGER NOT NULL,
    `day4` INTEGER NOT NULL,
    `day5` INTEGER NOT NULL,
    `day6` INTEGER NOT NULL,
    `day7` INTEGER NOT NULL,

    UNIQUE INDEX `tprg2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprg3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tprg2Id` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprg3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprg4` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprgId` BIGINT NULL,
    `tocrgId` BIGINT NULL,

    UNIQUE INDEX `tprg4_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprg5` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprgId` BIGINT NULL,
    `tocatId` BIGINT NULL,

    UNIQUE INDEX `tprg5_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprg9` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprgId` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprg9_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toprr` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `promocode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `startdate` DATE NOT NULL,
    `enddate` DATE NOT NULL,
    `starttime` TIME(0) NOT NULL,
    `endtime` TIME(0) NOT NULL,
    `remarks` TEXT NULL,
    `validdays` INTEGER NOT NULL,
    `minpurchase` DOUBLE NOT NULL,
    `minnextpurchase` DOUBLE NOT NULL,
    `minnextpurchasex` INTEGER NOT NULL,
    `vouchertype` VARCHAR(20) NOT NULL,
    `vouchervalue` DOUBLE NOT NULL,
    `voucherpct` DOUBLE NOT NULL,
    `statusactive` INTEGER NOT NULL,

    UNIQUE INDEX `toprr_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprr2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprrId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `holiday` INTEGER NOT NULL,
    `day1` INTEGER NOT NULL,
    `day2` INTEGER NOT NULL,
    `day3` INTEGER NOT NULL,
    `day4` INTEGER NOT NULL,
    `day5` INTEGER NOT NULL,
    `day6` INTEGER NOT NULL,
    `day7` INTEGER NOT NULL,

    UNIQUE INDEX `tprr2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprr3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tprr2Id` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprr3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprr4` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprrId` BIGINT NULL,
    `tocrgId` BIGINT NULL,

    UNIQUE INDEX `tprr4_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprr5` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprrId` BIGINT NULL,
    `tocatId` BIGINT NULL,

    UNIQUE INDEX `tprr5_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprr9` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprrId` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprr9_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toprn` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `couponcode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `startdate` DATE NOT NULL,
    `enddate` DATE NOT NULL,
    `starttime` TIME(0) NOT NULL,
    `endtime` TIME(0) NOT NULL,
    `remarks` TEXT NULL,
    `includepromo` INTEGER NOT NULL,
    `maxtimes` INTEGER NOT NULL,
    `minpurchase` DOUBLE NOT NULL,
    `generaldisc` DOUBLE NOT NULL,
    `maxgeneraldisc` VARCHAR(20) NOT NULL,
    `memberdisc` DOUBLE NOT NULL,
    `maxmemberdisc` DOUBLE NOT NULL,
    `statusactive` INTEGER NOT NULL,

    UNIQUE INDEX `toprn_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprn2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprnId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `holiday` INTEGER NOT NULL,
    `day1` INTEGER NOT NULL,
    `day2` INTEGER NOT NULL,
    `day3` INTEGER NOT NULL,
    `day4` INTEGER NOT NULL,
    `day5` INTEGER NOT NULL,
    `day6` INTEGER NOT NULL,
    `day7` INTEGER NOT NULL,

    UNIQUE INDEX `tprn2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprn3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tprn2Id` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprn3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprn4` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprnId` BIGINT NULL,
    `tocrgId` BIGINT NULL,

    UNIQUE INDEX `tprn4_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprn5` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprnId` BIGINT NULL,
    `tocatId` BIGINT NULL,

    UNIQUE INDEX `tprn5_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tprn9` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toprnId` BIGINT NULL,
    `day` INTEGER NOT NULL,
    `status` INTEGER NOT NULL,

    UNIQUE INDEX `tprn9_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tohld` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `calendarcode` VARCHAR(30) NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `descriptionfrgn` VARCHAR(200) NOT NULL,
    `fiscalyear` INTEGER NOT NULL,

    UNIQUE INDEX `tohld_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `thld1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tohldId` BIGINT NULL,
    `holidaydate` DATE NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `descriptionfrgn` VARCHAR(200) NOT NULL,

    UNIQUE INDEX `thld1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `toinv` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `docnum` VARCHAR(30) NOT NULL,
    `orderno` INTEGER NOT NULL,
    `tocusId` BIGINT NULL,
    `tohemId` BIGINT NULL,
    `transdate` DATE NOT NULL,
    `transtime` TIME(0) NOT NULL,
    `timezone` VARCHAR(200) NOT NULL,
    `remarks` TEXT NULL,
    `subtotal` DOUBLE NOT NULL,
    `discprctg` DOUBLE NOT NULL,
    `discamount` DOUBLE NOT NULL,
    `discountcard` DOUBLE NOT NULL,
    `coupon` VARCHAR(30) NOT NULL,
    `discountcoupon` DOUBLE NOT NULL,
    `taxprctg` DOUBLE NOT NULL,
    `taxamount` DOUBLE NOT NULL,
    `addcost` DOUBLE NOT NULL,
    `rounding` DOUBLE NOT NULL,
    `grandtotal` DOUBLE NOT NULL,
    `cash` DOUBLE NOT NULL,
    `card` DOUBLE NOT NULL,
    `extracharge` DOUBLE NOT NULL,
    `voucher` DOUBLE NOT NULL,
    `ar` DOUBLE NOT NULL,
    `changed` DOUBLE NOT NULL,
    `totalpayment` DOUBLE NOT NULL,
    `edc` VARCHAR(50) NOT NULL,
    `edccode` VARCHAR(20) NOT NULL,
    `cardno` VARCHAR(20) NOT NULL,
    `cardholder` VARCHAR(20) NOT NULL,
    `tocsrId` BIGINT NULL,
    `docstatus` INTEGER NOT NULL DEFAULT 0,
    `sync` INTEGER NOT NULL DEFAULT 0,
    `toinvTohemId` BIGINT NULL,

    UNIQUE INDEX `toinv_docid_key`(`docid`),
    UNIQUE INDEX `toinv_docnum_key`(`docnum`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tinv1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toinvId` BIGINT NULL,
    `linenum` INTEGER NOT NULL,
    `docnum` VARCHAR(30) NOT NULL,
    `idnumber` INTEGER NOT NULL,
    `toitmId` BIGINT NULL,
    `quantity` DOUBLE NOT NULL,
    `sellingprice` DOUBLE NOT NULL,
    `discprctg` DOUBLE NOT NULL,
    `discamount` DOUBLE NOT NULL,
    `totalamount` DOUBLE NOT NULL,
    `taxprctg` DOUBLE NOT NULL,
    `promotiontype` VARCHAR(20) NOT NULL,
    `promotionid` VARCHAR(191) NOT NULL,
    `remarks` TEXT NULL,
    `edittime` DATETIME(0) NOT NULL,
    `cogs` DOUBLE NOT NULL,
    `tovatId` BIGINT NULL,
    `promotiontingkat` VARCHAR(191) NULL,
    `promovoucherno` VARCHAR(191) NULL,
    `basedocid` VARCHAR(191) NULL,
    `baselinedocid` VARCHAR(191) NULL,
    `includetax` INTEGER NOT NULL,
    `tbitmId` BIGINT NULL,

    UNIQUE INDEX `tinv1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tinv2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NOT NULL,
    `toinvId` BIGINT NOT NULL,
    `linenum` INTEGER NOT NULL,
    `tpmt3Id` BIGINT NOT NULL,
    `amount` DOUBLE NOT NULL,
    `tpmt2Id` BIGINT NULL,
    `cardno` VARCHAR(20) NULL,
    `cardholder` VARCHAR(20) NULL,
    `sisavoucher` DOUBLE NULL,

    UNIQUE INDEX `tinv2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tinv3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tinv1Docid` VARCHAR(191) NULL,
    `toitmId` BIGINT NULL,
    `batchno` VARCHAR(10) NOT NULL,

    UNIQUE INDEX `tinv3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tinv4` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `toinvId` BIGINT NULL,
    `type` INTEGER NOT NULL,
    `serialno` VARCHAR(50) NOT NULL,
    `amount` DOUBLE NOT NULL,

    UNIQUE INDEX `tinv4_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `torin` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tostrId` BIGINT NULL,
    `docnum` VARCHAR(30) NOT NULL,
    `orderno` INTEGER NOT NULL,
    `tocusId` BIGINT NULL,
    `tohemId` BIGINT NULL,
    `transdate` DATE NOT NULL,
    `transtime` TIME(0) NOT NULL,
    `timezone` VARCHAR(200) NOT NULL,
    `remarks` TEXT NULL,
    `subtotal` DOUBLE NOT NULL,
    `discprctg` DOUBLE NOT NULL,
    `discamount` DOUBLE NOT NULL,
    `discountcard` DOUBLE NOT NULL,
    `coupon` VARCHAR(20) NOT NULL,
    `discountcoupon` DOUBLE NOT NULL,
    `taxprctg` DOUBLE NOT NULL,
    `taxamount` DOUBLE NOT NULL,
    `addcost` DOUBLE NOT NULL,
    `rounding` DOUBLE NOT NULL,
    `grandtotal` DOUBLE NOT NULL,
    `cash` DOUBLE NOT NULL,
    `card` DOUBLE NOT NULL,
    `extracharge` DOUBLE NOT NULL,
    `voucher` DOUBLE NOT NULL,
    `ar` DOUBLE NOT NULL,
    `changed` DOUBLE NOT NULL,
    `totalpayment` DOUBLE NOT NULL,
    `edc` VARCHAR(50) NOT NULL,
    `edccode` VARCHAR(20) NOT NULL,
    `cardno` VARCHAR(20) NOT NULL,
    `cardholder` VARCHAR(20) NOT NULL,
    `tocsrId` BIGINT NULL,
    `docstatus` INTEGER NOT NULL DEFAULT 0,
    `sync` INTEGER NOT NULL DEFAULT 0,
    `torinTohemId` BIGINT NULL,

    UNIQUE INDEX `torin_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `trin1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `torinId` BIGINT NULL,
    `linenum` INTEGER NOT NULL,
    `docnum` VARCHAR(30) NOT NULL,
    `idnumber` INTEGER NOT NULL,
    `toitmId` BIGINT NULL,
    `quantity` DOUBLE NOT NULL,
    `sellingprice` DOUBLE NOT NULL,
    `discprctg` DOUBLE NOT NULL,
    `discamount` DOUBLE NOT NULL,
    `totalamount` DOUBLE NOT NULL,
    `taxprctg` DOUBLE NOT NULL,
    `promotiontype` VARCHAR(20) NOT NULL,
    `promotionid` VARCHAR(191) NOT NULL,
    `remarks` TEXT NULL,
    `edittime` DATETIME(0) NOT NULL,
    `cogs` DOUBLE NOT NULL,
    `tovatId` BIGINT NULL,
    `promotiontingkat` VARCHAR(191) NULL,
    `promovoucherno` VARCHAR(191) NULL,
    `basedocid` VARCHAR(191) NULL,
    `baselinedocid` VARCHAR(191) NULL,
    `includetax` INTEGER NOT NULL,
    `tbitmId` BIGINT NULL,

    UNIQUE INDEX `trin1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `trin2` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `torinId` BIGINT NULL,
    `linenum` INTEGER NOT NULL,
    `tpmt3Id` BIGINT NULL,
    `amount` BIGINT NOT NULL,
    `tpmt2Id` BIGINT NULL,
    `cardno` VARCHAR(20) NULL,
    `cardholder` VARCHAR(20) NULL,
    `sisavoucher` DOUBLE NULL,

    UNIQUE INDEX `trin2_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `trin3` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `trin1Docid` VARCHAR(191) NULL,
    `toitmId` BIGINT NULL,
    `batchno` VARCHAR(10) NOT NULL,

    UNIQUE INDEX `trin3_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `golog` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `remarks` TEXT NULL,

    UNIQUE INDEX `golog_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tmpad` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `docnum` VARCHAR(30) NOT NULL,
    `docdate` DATE NOT NULL,
    `doctime` TIME(0) NOT NULL,
    `timezone` VARCHAR(200) NOT NULL,
    `posted` INTEGER NOT NULL,
    `postdate` DATE NOT NULL,
    `posttime` TIME(0) NOT NULL,
    `remarks` TEXT NULL,
    `tostrId` BIGINT NULL,

    UNIQUE INDEX `tmpad_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `mpad1` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `docid` VARCHAR(191) NOT NULL,
    `createdate` DATETIME(0) NOT NULL,
    `createby` BIGINT NOT NULL,
    `updatedate` DATETIME(0) NULL,
    `updateby` BIGINT NULL,
    `lastchanges` INTEGER NOT NULL DEFAULT 0,
    `gtentId` BIGINT NULL,
    `tmpadId` BIGINT NULL,
    `tpmt1Id` BIGINT NULL,
    `amount` DOUBLE NOT NULL,

    UNIQUE INDEX `mpad1_docid_key`(`docid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `tcurr` ADD CONSTRAINT `tcurr_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocry` ADD CONSTRAINT `tocry_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocry` ADD CONSTRAINT `tocry_tcurrId_fkey` FOREIGN KEY (`tcurrId`) REFERENCES `tcurr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toprv` ADD CONSTRAINT `toprv_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toprv` ADD CONSTRAINT `toprv_tocryId_fkey` FOREIGN KEY (`tocryId`) REFERENCES `tocry`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tozcd` ADD CONSTRAINT `tozcd_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tozcd` ADD CONSTRAINT `tozcd_toprvId_fkey` FOREIGN KEY (`toprvId`) REFERENCES `toprv`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tostr` ADD CONSTRAINT `tostr_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tostr` ADD CONSTRAINT `tostr_toprvId_fkey` FOREIGN KEY (`toprvId`) REFERENCES `toprv`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tostr` ADD CONSTRAINT `tostr_tocryId_fkey` FOREIGN KEY (`tocryId`) REFERENCES `tocry`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tostr` ADD CONSTRAINT `tostr_tozcdId_fkey` FOREIGN KEY (`tozcdId`) REFERENCES `tozcd`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tostr` ADD CONSTRAINT `tostr_tohemId_fkey` FOREIGN KEY (`tohemId`) REFERENCES `tohem`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tostr` ADD CONSTRAINT `tostr_tcurrId_fkey` FOREIGN KEY (`tcurrId`) REFERENCES `tcurr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tostr` ADD CONSTRAINT `tostr_toplnId_fkey` FOREIGN KEY (`toplnId`) REFERENCES `topln`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tostr` ADD CONSTRAINT `tostr_tovatId_fkey` FOREIGN KEY (`tovatId`) REFERENCES `tovat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tostr` ADD CONSTRAINT `tostr_credittaxcodeId_fkey` FOREIGN KEY (`credittaxcodeId`) REFERENCES `tovat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tostr` ADD CONSTRAINT `tostr_tpmt1Id_fkey` FOREIGN KEY (`tpmt1Id`) REFERENCES `tpmt1`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tohem` ADD CONSTRAINT `tohem_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tohem` ADD CONSTRAINT `tohem_toprvId_fkey` FOREIGN KEY (`toprvId`) REFERENCES `toprv`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tohem` ADD CONSTRAINT `tohem_tocryId_fkey` FOREIGN KEY (`tocryId`) REFERENCES `tocry`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tohem` ADD CONSTRAINT `tohem_tozcdId_fkey` FOREIGN KEY (`tozcdId`) REFERENCES `tozcd`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocsr` ADD CONSTRAINT `tocsr_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocsr` ADD CONSTRAINT `tocsr_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcsr1` ADD CONSTRAINT `tcsr1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcsr1` ADD CONSTRAINT `tcsr1_tocsrId_fkey` FOREIGN KEY (`tocsrId`) REFERENCES `tocsr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcsr1` ADD CONSTRAINT `tcsr1_tousrId_fkey` FOREIGN KEY (`tousrId`) REFERENCES `tousr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcsr1` ADD CONSTRAINT `tcsr1_openedbyId_fkey` FOREIGN KEY (`openedbyId`) REFERENCES `tousr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcsr1` ADD CONSTRAINT `tcsr1_closedbyId_fkey` FOREIGN KEY (`closedbyId`) REFERENCES `tousr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tousr` ADD CONSTRAINT `tousr_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tousr` ADD CONSTRAINT `tousr_tohemId_fkey` FOREIGN KEY (`tohemId`) REFERENCES `tohem`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tousr` ADD CONSTRAINT `tousr_torolId_fkey` FOREIGN KEY (`torolId`) REFERENCES `torol`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tousr` ADD CONSTRAINT `tousr_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `torol` ADD CONSTRAINT `torol_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toaut` ADD CONSTRAINT `toaut_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toaut` ADD CONSTRAINT `toaut_tformId_fkey` FOREIGN KEY (`tformId`) REFERENCES `tform`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toaut` ADD CONSTRAINT `toaut_tousrId_fkey` FOREIGN KEY (`tousrId`) REFERENCES `tousr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocat` ADD CONSTRAINT `tocat_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocat` ADD CONSTRAINT `tocat_parentId_fkey` FOREIGN KEY (`parentId`) REFERENCES `tocat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocat` ADD CONSTRAINT `tocat_phir1Id_fkey` FOREIGN KEY (`phir1Id`) REFERENCES `phir1`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `touom` ADD CONSTRAINT `touom_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_tocatId_fkey` FOREIGN KEY (`tocatId`) REFERENCES `tocat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_touomId_fkey` FOREIGN KEY (`touomId`) REFERENCES `touom`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_property1_fkey` FOREIGN KEY (`property1`) REFERENCES `tprop`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_property2_fkey` FOREIGN KEY (`property2`) REFERENCES `tprop`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_property3_fkey` FOREIGN KEY (`property3`) REFERENCES `tprop`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_property4_fkey` FOREIGN KEY (`property4`) REFERENCES `tprop`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_property5_fkey` FOREIGN KEY (`property5`) REFERENCES `tprop`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_property6_fkey` FOREIGN KEY (`property6`) REFERENCES `tprop`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_property7_fkey` FOREIGN KEY (`property7`) REFERENCES `tprop`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_property8_fkey` FOREIGN KEY (`property8`) REFERENCES `tprop`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_property9_fkey` FOREIGN KEY (`property9`) REFERENCES `tprop`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitm` ADD CONSTRAINT `toitm_property10_fkey` FOREIGN KEY (`property10`) REFERENCES `tprop`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tbitm` ADD CONSTRAINT `tbitm_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tbitm` ADD CONSTRAINT `tbitm_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tbitm` ADD CONSTRAINT `tbitm_touomId_fkey` FOREIGN KEY (`touomId`) REFERENCES `touom`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tsitm` ADD CONSTRAINT `tsitm_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tsitm` ADD CONSTRAINT `tsitm_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tsitm` ADD CONSTRAINT `tsitm_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tsitm` ADD CONSTRAINT `tsitm_tovatId_fkey` FOREIGN KEY (`tovatId`) REFERENCES `tovat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tsitm` ADD CONSTRAINT `tsitm_tovatIdPur_fkey` FOREIGN KEY (`tovatIdPur`) REFERENCES `tovat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tvitm` ADD CONSTRAINT `tvitm_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tvitm` ADD CONSTRAINT `tvitm_tsitmId_fkey` FOREIGN KEY (`tsitmId`) REFERENCES `tsitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpitm` ADD CONSTRAINT `tpitm_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpitm` ADD CONSTRAINT `tpitm_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tritm` ADD CONSTRAINT `tritm_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tritm` ADD CONSTRAINT `tritm_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `topln` ADD CONSTRAINT `topln_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `topln` ADD CONSTRAINT `topln_tcurrId_fkey` FOREIGN KEY (`tcurrId`) REFERENCES `tcurr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln1` ADD CONSTRAINT `tpln1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln1` ADD CONSTRAINT `tpln1_toplnId_fkey` FOREIGN KEY (`toplnId`) REFERENCES `topln`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln2` ADD CONSTRAINT `tpln2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln2` ADD CONSTRAINT `tpln2_tpln1Id_fkey` FOREIGN KEY (`tpln1Id`) REFERENCES `tpln1`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln2` ADD CONSTRAINT `tpln2_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln2` ADD CONSTRAINT `tpln2_tcurrId_fkey` FOREIGN KEY (`tcurrId`) REFERENCES `tcurr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln3` ADD CONSTRAINT `tpln3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln3` ADD CONSTRAINT `tpln3_toplnId_fkey` FOREIGN KEY (`toplnId`) REFERENCES `topln`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln3` ADD CONSTRAINT `tpln3_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln4` ADD CONSTRAINT `tpln4_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln4` ADD CONSTRAINT `tpln4_tpln2Id_fkey` FOREIGN KEY (`tpln2Id`) REFERENCES `tpln2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpln4` ADD CONSTRAINT `tpln4_tbitmId_fkey` FOREIGN KEY (`tbitmId`) REFERENCES `tbitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tobpt` ADD CONSTRAINT `tobpt_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `togen` ADD CONSTRAINT `togen_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocrg` ADD CONSTRAINT `tocrg_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocus` ADD CONSTRAINT `tocus_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocus` ADD CONSTRAINT `tocus_tocrgId_fkey` FOREIGN KEY (`tocrgId`) REFERENCES `tocrg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocus` ADD CONSTRAINT `tocus_toprvId_fkey` FOREIGN KEY (`toprvId`) REFERENCES `toprv`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocus` ADD CONSTRAINT `tocus_tocryId_fkey` FOREIGN KEY (`tocryId`) REFERENCES `tocry`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocus` ADD CONSTRAINT `tocus_tozcdId_fkey` FOREIGN KEY (`tozcdId`) REFERENCES `tozcd`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocus` ADD CONSTRAINT `tocus_toptrId_fkey` FOREIGN KEY (`toptrId`) REFERENCES `toptr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocus` ADD CONSTRAINT `tocus_toplnId_fkey` FOREIGN KEY (`toplnId`) REFERENCES `topln`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tocus` ADD CONSTRAINT `tocus_tohemId_fkey` FOREIGN KEY (`tohemId`) REFERENCES `tohem`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcus1` ADD CONSTRAINT `tcus1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcus1` ADD CONSTRAINT `tcus1_tocusId_fkey` FOREIGN KEY (`tocusId`) REFERENCES `tocus`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcus1` ADD CONSTRAINT `tcus1_toprvId_fkey` FOREIGN KEY (`toprvId`) REFERENCES `toprv`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcus1` ADD CONSTRAINT `tcus1_tocryId_fkey` FOREIGN KEY (`tocryId`) REFERENCES `tocry`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcus1` ADD CONSTRAINT `tcus1_tozcdId_fkey` FOREIGN KEY (`tozcdId`) REFERENCES `tozcd`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcus2` ADD CONSTRAINT `tcus2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tcus2` ADD CONSTRAINT `tcus2_tocusId_fkey` FOREIGN KEY (`tocusId`) REFERENCES `tocus`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toptr` ADD CONSTRAINT `toptr_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `topmt` ADD CONSTRAINT `topmt_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpmt1` ADD CONSTRAINT `tpmt1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpmt1` ADD CONSTRAINT `tpmt1_topmtId_fkey` FOREIGN KEY (`topmtId`) REFERENCES `topmt`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpmt2` ADD CONSTRAINT `tpmt2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpmt3` ADD CONSTRAINT `tpmt3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpmt3` ADD CONSTRAINT `tpmt3_tpmt1Id_fkey` FOREIGN KEY (`tpmt1Id`) REFERENCES `tpmt1`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tpmt3` ADD CONSTRAINT `tpmt3_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tobnk` ADD CONSTRAINT `tobnk_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tobnk` ADD CONSTRAINT `tobnk_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitt` ADD CONSTRAINT `toitt_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitt` ADD CONSTRAINT `toitt_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toitt` ADD CONSTRAINT `toitt_touomId_fkey` FOREIGN KEY (`touomId`) REFERENCES `touom`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `titt1` ADD CONSTRAINT `titt1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `titt1` ADD CONSTRAINT `titt1_toittId_fkey` FOREIGN KEY (`toittId`) REFERENCES `toitt`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `titt1` ADD CONSTRAINT `titt1_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `titt1` ADD CONSTRAINT `titt1_touomId_fkey` FOREIGN KEY (`touomId`) REFERENCES `touom`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tovat` ADD CONSTRAINT `tovat_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `taut1` ADD CONSTRAINT `taut1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `taut1` ADD CONSTRAINT `taut1_tformId_fkey` FOREIGN KEY (`tformId`) REFERENCES `tform`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `taut1` ADD CONSTRAINT `taut1_torolId_fkey` FOREIGN KEY (`torolId`) REFERENCES `torol`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tform` ADD CONSTRAINT `tform_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tphir` ADD CONSTRAINT `tphir_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `phir1` ADD CONSTRAINT `phir1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `phir1` ADD CONSTRAINT `phir1_tphirId_fkey` FOREIGN KEY (`tphirId`) REFERENCES `tphir`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprop` ADD CONSTRAINT `tprop_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toprp` ADD CONSTRAINT `toprp_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toprp` ADD CONSTRAINT `toprp_toplnId_fkey` FOREIGN KEY (`toplnId`) REFERENCES `topln`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp1` ADD CONSTRAINT `tprp1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp1` ADD CONSTRAINT `tprp1_toprpId_fkey` FOREIGN KEY (`toprpId`) REFERENCES `toprp`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp1` ADD CONSTRAINT `tprp1_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp2` ADD CONSTRAINT `tprp2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp2` ADD CONSTRAINT `tprp2_toprpId_fkey` FOREIGN KEY (`toprpId`) REFERENCES `toprp`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp2` ADD CONSTRAINT `tprp2_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp3` ADD CONSTRAINT `tprp3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp3` ADD CONSTRAINT `tprp3_tprp2Id_fkey` FOREIGN KEY (`tprp2Id`) REFERENCES `tprp2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp4` ADD CONSTRAINT `tprp4_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp4` ADD CONSTRAINT `tprp4_toprpId_fkey` FOREIGN KEY (`toprpId`) REFERENCES `toprp`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp4` ADD CONSTRAINT `tprp4_tocrgId_fkey` FOREIGN KEY (`tocrgId`) REFERENCES `tocrg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp8` ADD CONSTRAINT `tprp8_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp8` ADD CONSTRAINT `tprp8_toprpId_fkey` FOREIGN KEY (`toprpId`) REFERENCES `toprp`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp9` ADD CONSTRAINT `tprp9_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprp9` ADD CONSTRAINT `tprp9_toprpId_fkey` FOREIGN KEY (`toprpId`) REFERENCES `toprp`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toprc` ADD CONSTRAINT `toprc_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc1` ADD CONSTRAINT `tprc1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc1` ADD CONSTRAINT `tprc1_toprcId_fkey` FOREIGN KEY (`toprcId`) REFERENCES `toprc`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc1` ADD CONSTRAINT `tprc1_tpmt2Id_fkey` FOREIGN KEY (`tpmt2Id`) REFERENCES `tpmt2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc2` ADD CONSTRAINT `tprc2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc2` ADD CONSTRAINT `tprc2_toprcId_fkey` FOREIGN KEY (`toprcId`) REFERENCES `toprc`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc2` ADD CONSTRAINT `tprc2_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc3` ADD CONSTRAINT `tprc3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc3` ADD CONSTRAINT `tprc3_tprc2Id_fkey` FOREIGN KEY (`tprc2Id`) REFERENCES `tprc2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc4` ADD CONSTRAINT `tprc4_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc4` ADD CONSTRAINT `tprc4_toprcId_fkey` FOREIGN KEY (`toprcId`) REFERENCES `toprc`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc4` ADD CONSTRAINT `tprc4_tocrgId_fkey` FOREIGN KEY (`tocrgId`) REFERENCES `tocrg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc5` ADD CONSTRAINT `tprc5_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc5` ADD CONSTRAINT `tprc5_toprcId_fkey` FOREIGN KEY (`toprcId`) REFERENCES `toprc`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc5` ADD CONSTRAINT `tprc5_tocatId_fkey` FOREIGN KEY (`tocatId`) REFERENCES `tocrg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc9` ADD CONSTRAINT `tprc9_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprc9` ADD CONSTRAINT `tprc9_toprcId_fkey` FOREIGN KEY (`toprcId`) REFERENCES `toprc`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toprb` ADD CONSTRAINT `toprb_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb1` ADD CONSTRAINT `tprb1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb1` ADD CONSTRAINT `tprb1_toprbId_fkey` FOREIGN KEY (`toprbId`) REFERENCES `toprb`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb1` ADD CONSTRAINT `tprb1_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb2` ADD CONSTRAINT `tprb2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb2` ADD CONSTRAINT `tprb2_toprbId_fkey` FOREIGN KEY (`toprbId`) REFERENCES `toprb`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb2` ADD CONSTRAINT `tprb2_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb3` ADD CONSTRAINT `tprb3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb3` ADD CONSTRAINT `tprb3_tprb2Id_fkey` FOREIGN KEY (`tprb2Id`) REFERENCES `tprb2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb4` ADD CONSTRAINT `tprb4_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb4` ADD CONSTRAINT `tprb4_toprbId_fkey` FOREIGN KEY (`toprbId`) REFERENCES `toprb`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb4` ADD CONSTRAINT `tprb4_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb5` ADD CONSTRAINT `tprb5_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb5` ADD CONSTRAINT `tprb5_toprbId_fkey` FOREIGN KEY (`toprbId`) REFERENCES `toprb`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb5` ADD CONSTRAINT `tprb5_tocrgId_fkey` FOREIGN KEY (`tocrgId`) REFERENCES `tocrg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb6` ADD CONSTRAINT `tprb6_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb6` ADD CONSTRAINT `tprb6_toprbId_fkey` FOREIGN KEY (`toprbId`) REFERENCES `toprb`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb6` ADD CONSTRAINT `tprb6_tocatId_fkey` FOREIGN KEY (`tocatId`) REFERENCES `tocat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb6` ADD CONSTRAINT `tprb6_tocrgId_fkey` FOREIGN KEY (`tocrgId`) REFERENCES `tocrg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb9` ADD CONSTRAINT `tprb9_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprb9` ADD CONSTRAINT `tprb9_toprbId_fkey` FOREIGN KEY (`toprbId`) REFERENCES `toprb`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toprk` ADD CONSTRAINT `toprk_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk1` ADD CONSTRAINT `tprk1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk1` ADD CONSTRAINT `tprk1_toprkId_fkey` FOREIGN KEY (`toprkId`) REFERENCES `toprk`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk1` ADD CONSTRAINT `tprk1_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk2` ADD CONSTRAINT `tprk2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk2` ADD CONSTRAINT `tprk2_toprkId_fkey` FOREIGN KEY (`toprkId`) REFERENCES `toprk`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk2` ADD CONSTRAINT `tprk2_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk3` ADD CONSTRAINT `tprk3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk3` ADD CONSTRAINT `tprk3_tprk2Id_fkey` FOREIGN KEY (`tprk2Id`) REFERENCES `tprk2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk4` ADD CONSTRAINT `tprk4_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk4` ADD CONSTRAINT `tprk4_toprkId_fkey` FOREIGN KEY (`toprkId`) REFERENCES `toprk`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk4` ADD CONSTRAINT `tprk4_tocrgId_fkey` FOREIGN KEY (`tocrgId`) REFERENCES `tocrg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk5` ADD CONSTRAINT `tprk5_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk5` ADD CONSTRAINT `tprk5_toprkId_fkey` FOREIGN KEY (`toprkId`) REFERENCES `toprk`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk5` ADD CONSTRAINT `tprk5_tocatId_fkey` FOREIGN KEY (`tocatId`) REFERENCES `tocat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk9` ADD CONSTRAINT `tprk9_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprk9` ADD CONSTRAINT `tprk9_toprkId_fkey` FOREIGN KEY (`toprkId`) REFERENCES `toprk`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toprg` ADD CONSTRAINT `toprg_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg1` ADD CONSTRAINT `tprg1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg1` ADD CONSTRAINT `tprg1_toprgId_fkey` FOREIGN KEY (`toprgId`) REFERENCES `toprg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg1` ADD CONSTRAINT `tprg1_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg2` ADD CONSTRAINT `tprg2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg2` ADD CONSTRAINT `tprg2_toprgId_fkey` FOREIGN KEY (`toprgId`) REFERENCES `toprg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg2` ADD CONSTRAINT `tprg2_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg3` ADD CONSTRAINT `tprg3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg3` ADD CONSTRAINT `tprg3_tprg2Id_fkey` FOREIGN KEY (`tprg2Id`) REFERENCES `tprg2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg4` ADD CONSTRAINT `tprg4_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg4` ADD CONSTRAINT `tprg4_toprgId_fkey` FOREIGN KEY (`toprgId`) REFERENCES `toprg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg4` ADD CONSTRAINT `tprg4_tocrgId_fkey` FOREIGN KEY (`tocrgId`) REFERENCES `tocrg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg5` ADD CONSTRAINT `tprg5_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg5` ADD CONSTRAINT `tprg5_toprgId_fkey` FOREIGN KEY (`toprgId`) REFERENCES `toprg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg5` ADD CONSTRAINT `tprg5_tocatId_fkey` FOREIGN KEY (`tocatId`) REFERENCES `tocat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg9` ADD CONSTRAINT `tprg9_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprg9` ADD CONSTRAINT `tprg9_toprgId_fkey` FOREIGN KEY (`toprgId`) REFERENCES `toprg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toprr` ADD CONSTRAINT `toprr_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr2` ADD CONSTRAINT `tprr2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr2` ADD CONSTRAINT `tprr2_toprrId_fkey` FOREIGN KEY (`toprrId`) REFERENCES `toprr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr2` ADD CONSTRAINT `tprr2_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr3` ADD CONSTRAINT `tprr3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr3` ADD CONSTRAINT `tprr3_tprr2Id_fkey` FOREIGN KEY (`tprr2Id`) REFERENCES `tprr2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr4` ADD CONSTRAINT `tprr4_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr4` ADD CONSTRAINT `tprr4_toprrId_fkey` FOREIGN KEY (`toprrId`) REFERENCES `toprr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr4` ADD CONSTRAINT `tprr4_tocrgId_fkey` FOREIGN KEY (`tocrgId`) REFERENCES `tocrg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr5` ADD CONSTRAINT `tprr5_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr5` ADD CONSTRAINT `tprr5_toprrId_fkey` FOREIGN KEY (`toprrId`) REFERENCES `toprr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr5` ADD CONSTRAINT `tprr5_tocatId_fkey` FOREIGN KEY (`tocatId`) REFERENCES `tocat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr9` ADD CONSTRAINT `tprr9_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprr9` ADD CONSTRAINT `tprr9_toprrId_fkey` FOREIGN KEY (`toprrId`) REFERENCES `toprr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toprn` ADD CONSTRAINT `toprn_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn2` ADD CONSTRAINT `tprn2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn2` ADD CONSTRAINT `tprn2_toprnId_fkey` FOREIGN KEY (`toprnId`) REFERENCES `toprn`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn2` ADD CONSTRAINT `tprn2_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn3` ADD CONSTRAINT `tprn3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn3` ADD CONSTRAINT `tprn3_tprn2Id_fkey` FOREIGN KEY (`tprn2Id`) REFERENCES `tprn2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn4` ADD CONSTRAINT `tprn4_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn4` ADD CONSTRAINT `tprn4_toprnId_fkey` FOREIGN KEY (`toprnId`) REFERENCES `toprn`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn4` ADD CONSTRAINT `tprn4_tocrgId_fkey` FOREIGN KEY (`tocrgId`) REFERENCES `tocrg`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn5` ADD CONSTRAINT `tprn5_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn5` ADD CONSTRAINT `tprn5_toprnId_fkey` FOREIGN KEY (`toprnId`) REFERENCES `toprn`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn5` ADD CONSTRAINT `tprn5_tocatId_fkey` FOREIGN KEY (`tocatId`) REFERENCES `tocat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn9` ADD CONSTRAINT `tprn9_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tprn9` ADD CONSTRAINT `tprn9_toprnId_fkey` FOREIGN KEY (`toprnId`) REFERENCES `toprn`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tohld` ADD CONSTRAINT `tohld_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `thld1` ADD CONSTRAINT `thld1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `thld1` ADD CONSTRAINT `thld1_tohldId_fkey` FOREIGN KEY (`tohldId`) REFERENCES `tohld`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toinv` ADD CONSTRAINT `toinv_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toinv` ADD CONSTRAINT `toinv_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toinv` ADD CONSTRAINT `toinv_tocusId_fkey` FOREIGN KEY (`tocusId`) REFERENCES `tocus`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toinv` ADD CONSTRAINT `toinv_tohemId_fkey` FOREIGN KEY (`tohemId`) REFERENCES `tohem`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toinv` ADD CONSTRAINT `toinv_tocsrId_fkey` FOREIGN KEY (`tocsrId`) REFERENCES `tocsr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `toinv` ADD CONSTRAINT `toinv_toinvTohemId_fkey` FOREIGN KEY (`toinvTohemId`) REFERENCES `tohem`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv1` ADD CONSTRAINT `tinv1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv1` ADD CONSTRAINT `tinv1_toinvId_fkey` FOREIGN KEY (`toinvId`) REFERENCES `toinv`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv1` ADD CONSTRAINT `tinv1_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv1` ADD CONSTRAINT `tinv1_tovatId_fkey` FOREIGN KEY (`tovatId`) REFERENCES `tovat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv1` ADD CONSTRAINT `tinv1_tbitmId_fkey` FOREIGN KEY (`tbitmId`) REFERENCES `tbitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv2` ADD CONSTRAINT `tinv2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv2` ADD CONSTRAINT `tinv2_toinvId_fkey` FOREIGN KEY (`toinvId`) REFERENCES `toinv`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv2` ADD CONSTRAINT `tinv2_tpmt3Id_fkey` FOREIGN KEY (`tpmt3Id`) REFERENCES `tpmt3`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv2` ADD CONSTRAINT `tinv2_tpmt2Id_fkey` FOREIGN KEY (`tpmt2Id`) REFERENCES `tpmt2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv3` ADD CONSTRAINT `tinv3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv3` ADD CONSTRAINT `tinv3_tinv1Docid_fkey` FOREIGN KEY (`tinv1Docid`) REFERENCES `tinv1`(`docid`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv3` ADD CONSTRAINT `tinv3_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv4` ADD CONSTRAINT `tinv4_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tinv4` ADD CONSTRAINT `tinv4_toinvId_fkey` FOREIGN KEY (`toinvId`) REFERENCES `toinv`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `torin` ADD CONSTRAINT `torin_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `torin` ADD CONSTRAINT `torin_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `torin` ADD CONSTRAINT `torin_tocusId_fkey` FOREIGN KEY (`tocusId`) REFERENCES `tocus`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `torin` ADD CONSTRAINT `torin_tohemId_fkey` FOREIGN KEY (`tohemId`) REFERENCES `tohem`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `torin` ADD CONSTRAINT `torin_tocsrId_fkey` FOREIGN KEY (`tocsrId`) REFERENCES `tocsr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `torin` ADD CONSTRAINT `torin_torinTohemId_fkey` FOREIGN KEY (`torinTohemId`) REFERENCES `tohem`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin1` ADD CONSTRAINT `trin1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin1` ADD CONSTRAINT `trin1_torinId_fkey` FOREIGN KEY (`torinId`) REFERENCES `torin`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin1` ADD CONSTRAINT `trin1_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin1` ADD CONSTRAINT `trin1_tovatId_fkey` FOREIGN KEY (`tovatId`) REFERENCES `tovat`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin1` ADD CONSTRAINT `trin1_tbitmId_fkey` FOREIGN KEY (`tbitmId`) REFERENCES `tbitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin2` ADD CONSTRAINT `trin2_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin2` ADD CONSTRAINT `trin2_torinId_fkey` FOREIGN KEY (`torinId`) REFERENCES `torin`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin2` ADD CONSTRAINT `trin2_tpmt3Id_fkey` FOREIGN KEY (`tpmt3Id`) REFERENCES `tpmt3`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin2` ADD CONSTRAINT `trin2_tpmt2Id_fkey` FOREIGN KEY (`tpmt2Id`) REFERENCES `tpmt2`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin3` ADD CONSTRAINT `trin3_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin3` ADD CONSTRAINT `trin3_trin1Docid_fkey` FOREIGN KEY (`trin1Docid`) REFERENCES `trin1`(`docid`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `trin3` ADD CONSTRAINT `trin3_toitmId_fkey` FOREIGN KEY (`toitmId`) REFERENCES `toitm`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `golog` ADD CONSTRAINT `golog_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tmpad` ADD CONSTRAINT `tmpad_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tmpad` ADD CONSTRAINT `tmpad_tostrId_fkey` FOREIGN KEY (`tostrId`) REFERENCES `tostr`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `mpad1` ADD CONSTRAINT `mpad1_gtentId_fkey` FOREIGN KEY (`gtentId`) REFERENCES `gtent`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `mpad1` ADD CONSTRAINT `mpad1_tmpadId_fkey` FOREIGN KEY (`tmpadId`) REFERENCES `tmpad`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `mpad1` ADD CONSTRAINT `mpad1_tpmt1Id_fkey` FOREIGN KEY (`tpmt1Id`) REFERENCES `tpmt1`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
