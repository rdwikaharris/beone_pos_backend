export function accessCodeConverter(code: string) {
  let builder: any = null;

  // * Check View
  if (code.slice(0, 1) === "1") {
    builder = {
      ...builder,
      canview: 1,
    };
  }

  // * Check Create
  if (code.slice(1, 2) === "1") {
    builder = {
      ...builder,
      cancreate: 1,
    };
  }

  // * Check Update
  if (code.slice(2, 3) === "1") {
    builder = {
      ...builder,
      canupdate: 1,
    };
  }

  return builder;
}
