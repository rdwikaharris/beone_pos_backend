import axios, { AxiosError } from "axios";
import { Request, Response } from "express";
import { JsonWebTokenError } from "jsonwebtoken";
import { ResultObject } from "../interface";
import { inspect } from "util";
import moment from "moment";

let resultObject: ResultObject;

export function errorConsole(data: any): void {
  console.log(moment().format());
  console.log(data);
}

export function devConsole(data: any): void {
  console.log(moment().format());
  console.log(data);
}

export function consoleLog(data: any): void {
  console.log(data);
}

export function consoleDir(data: any): void {
  console.dir(data);
}

export function consoleInspect(data: any): void {
  console.log(inspect(data, { showHidden: false, depth: null, colors: true }));
}

export function catchError(err: any, res: Response) {
  console.error(err);

  if (axios.isAxiosError(err)) {
    const axiosError: AxiosError = err;
    if (axiosError.response?.status === 401) {
      resultObject = {
        statuscode: 401,
        data: {
          description: `Unauthorized access. Please login again`,
        },
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } else {
      console.error(
        "An error occurred while making the request:",
        axiosError.message
      );
    }
  }

  if (err instanceof JsonWebTokenError) {
    resultObject = {
      statuscode: 401,
      data: {
        description: `Invalid token or expired`,
      },
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  }

  resultObject = {
    statuscode: 500,
    data: {
      description: `Something Went Wrong`,
    },
  };
  return res.status(resultObject.statuscode).json(resultObject.data);
}
