import jwt from "jsonwebtoken";
import { ENV } from "../constant/env";

export function decodeToken(token: string) {
  return jwt.verify(
    token.replace("Bearer ", ""),
    String(ENV.SECRET_KEY)
  ) as any;
}

export function tokenJWT(data: object) {
  return jwt.sign(data, ENV.SECRET_KEY as string, {
    algorithm: "HS256",
    expiresIn: "7d",
  });
}
