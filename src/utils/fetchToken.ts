import axios from "axios";

const fetchBosToken = async (email: String, password: String) => {
  try {
    const { data } = await axios.post("http://localhost:3001/auth/login", {
      email,
      password,
    });
    return data.token;
  } catch (err) {
    console.error("Error fetching", err);
  }
};

export default fetchBosToken;
