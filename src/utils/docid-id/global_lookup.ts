import { prisma } from "../../config/prisma";
import { DOCID_TRIGGER_ERROR } from "../../constant/paging";

const triggerError = DOCID_TRIGGER_ERROR;

/*
 * docid to id converter
 */

export async function gtentData(documentID: string) {
  const { id }: any = await prisma.gtent.findUnique({
    where: { docid: documentID },
    select: { id: true },
  });

  return Number(id);
}

// export async function gcmszData(documentID: string) {
//   const { id }: any = await prisma.gcmsz.findUnique({
//     where: { docid: documentID },
//     select: { id: true },
//   });

//   return Number(id);
// }

// export async function glindData(documentID: string) {
//   const { id }: any = await prisma.glind.findUnique({
//     where: { docid: documentID },
//     select: { id: true },
//   });

//   return Number(id);
// }

// export async function gformData(documentID: string) {
//   const { id }: any = await prisma.gform.findUnique({
//     where: { docid: documentID },
//     select: { id: true },
//   });

//   return Number(id);
// }

// export async function gcntrData(documentID: string) {
//   const { id }: any = await prisma.gcntr.findUnique({
//     where: { docid: documentID },
//     select: { id: true },
//   });

//   return Number(id);
// }

// export async function gprvnData(documentID: string) {
//   const { id }: any = await prisma.gprvn.findUnique({
//     where: { docid: documentID },
//     select: { id: true },
//   });

//   return Number(id);
// }

// export async function gzipcData(documentID: string) {
//   const { id }: any = await prisma.gzipc.findUnique({
//     where: { docid: documentID },
//     select: { id: true },
//   });

//   return Number(id);
// }

// export async function gcurrData(documentID: string) {
//   const { id }: any = await prisma.gcurr.findUnique({
//     where: { docid: documentID },
//     select: { id: true },
//   });

//   return Number(id);
// }

export async function gopmtData(documentID: string) {
  const { id }: any = await prisma.gopmt.findUnique({
    where: { docid: documentID },
    select: { id: true },
  });

  return Number(id);
}

export async function gologData(documentID: string) {
  const { id }: any = await prisma.golog.findUnique({
    where: { docid: documentID },
    select: { id: true },
  });

  return Number(id);
}
