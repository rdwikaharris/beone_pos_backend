import { prisma } from "../../config/prisma";
import { DOCID_TRIGGER_ERROR } from "../../constant/paging";

const triggerError = DOCID_TRIGGER_ERROR;

// * 1 - 5

export async function tcurrData(documentID: string, token: any) {
  const { id }: any = await prisma.tcurr.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tocryData(documentID: string, token: any) {
  const { id }: any = await prisma.tocry.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function toprvData(documentID: string, token: any) {
  const { id }: any = await prisma.toprv.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tozcdData(documentID: string, token: any) {
  const { id }: any = await prisma.tozcd.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tostrData(documentID: string, token: any) {
  const { id }: any = await prisma.tostr.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

// * 6 - 10

export async function tohemData(documentID: string, token: any) {
  const { id }: any = await prisma.tohem.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tocsrData(documentID: string, token: any) {
  const { id }: any = await prisma.tocsr.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tousrData(documentID: string, token: any) {
  const { id }: any = await prisma.tousr.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function torolData(documentID: string, token: any) {
  const { id }: any = await prisma.torol.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function toautData(documentID: string, token: any) {
  const { id }: any = await prisma.toaut.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

// * 11 - 15

export async function taut1Data(documentID: string, token: any) {
  const { id }: any = await prisma.taut1.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tocatData(documentID: string, token: any) {
  const { id }: any = await prisma.tocat.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tphirData(documentID: string, token: any) {
  const { id }: any = await prisma.tphir.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function phir1Data(documentID: string, token: any) {
  const { id }: any = await prisma.phir1.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function toitmData(documentID: string, token: any) {
  const { id }: any = await prisma.toitm.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

// * 16 - 20

export async function tbitmData(documentID: string, token: any) {
  const { id }: any = await prisma.tbitm.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tsitmData(documentID: string, token: any) {
  const { id }: any = await prisma.tsitm.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tvitmData(documentID: string, token: any) {
  const { id }: any = await prisma.tvitm.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function touomData(documentID: string, token: any) {
  const { id }: any = await prisma.touom.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tpitmData(documentID: string, token: any) {
  const { id }: any = await prisma.tpitm.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

// * 21 - 25

export async function toplnData(documentID: string, token: any) {
  const { id }: any = await prisma.topln.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tpln1Data(documentID: string, token: any) {
  const { id }: any = await prisma.tpln1.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tpln2Data(documentID: string, token: any) {
  const { id }: any = await prisma.tpln2.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tformData(documentID: string, token: any) {
  const { id }: any = await prisma.tform.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tocrgData(documentID: string, token: any) {
  const { id }: any = await prisma.tocrg.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

// * 26 - 30

// export async function tovdgData(documentID: string, token: any) {
//   const { id }: any = await prisma.tovdg.findFirst({
//     where: {
//       docid: documentID ? documentID : triggerError,
//       gtent_id: { docid: token },
//     },
//     select: { id: true },
//   });

//   return Number(id);
// }

export async function tocusData(documentID: string, token: any) {
  const { id }: any = await prisma.tocus.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tcus1Data(documentID: string, token: any) {
  const { id }: any = await prisma.tcus1.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tcus2Data(documentID: string, token: any) {
  const { id }: any = await prisma.tcus2.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

// export async function tovenData(documentID: string, token: any) {
//   const { id }: any = await prisma.toven.findFirst({
//     where: {
//       docid: documentID ? documentID : triggerError,
//       gtent_id: { docid: token },
//     },
//     select: { id: true },
//   });

//   return Number(id);
// }

// * 31 - 35

// export async function tven1Data(documentID: string, token: any) {
//   const { id }: any = await prisma.tven1.findFirst({
//     where: {
//       docid: documentID ? documentID : triggerError,
//       gtent_id: { docid: token },
//     },
//     select: { id: true },
//   });

//   return Number(id);
// }

// export async function tven2Data(documentID: string, token: any) {
//   const { id }: any = await prisma.tven2.findFirst({
//     where: {
//       docid: documentID ? documentID : triggerError,
//       gtent_id: { docid: token },
//     },
//     select: { id: true },
//   });

//   return Number(id);
// }

export async function toptrData(documentID: string, token: any) {
  const { id }: any = await prisma.toptr.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tpln3Data(documentID: string, token: any) {
  const { id }: any = await prisma.tpln3.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function topmtData(documentID: string, token: any) {
  const { id }: any = await prisma.topmt.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

// * 36 - 40

export async function tpmt1Data(documentID: string, token: any) {
  const { id }: any = await prisma.tpmt1.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tpmt2Data(documentID: string, token: any) {
  const { id }: any = await prisma.tpmt2.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tobnkData(documentID: string, token: any) {
  const { id }: any = await prisma.tobnk.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function toittData(documentID: string, token: any) {
  const { id }: any = await prisma.toitt.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function titt1Data(documentID: string, token: any) {
  const { id }: any = await prisma.titt1.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

// * 41 - 45

export async function tritmData(documentID: string, token: any) {
  const { id }: any = await prisma.tritm.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

// export async function toexpData(documentID: string, token: any) {
//   const { id }: any = await prisma.toexp.findFirst({
//     where: {
//       docid: documentID ? documentID : triggerError,
//       gtent_id: { docid: token },
//     },
//     select: { id: true },
//   });

//   return Number(id);
// }

// export async function toincData(documentID: string, token: any) {
//   const { id }: any = await prisma.toinc.findFirst({
//     where: {
//       docid: documentID ? documentID : triggerError,
//       gtent_id: { docid: token },
//     },
//     select: { id: true },
//   });

//   return Number(id);
// }

export async function tovatData(documentID: string, token: any) {
  const { id }: any = await prisma.tovat.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function toprpData(documentID: string, token: any) {
  const { id }: any = await prisma.toprp.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

// * 46 - 50

export async function tprp1Data(documentID: string, token: any) {
  const { id }: any = await prisma.tprp1.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tprp2Data(documentID: string, token: any) {
  const { id }: any = await prisma.tprp2.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tprp3Data(documentID: string, token: any) {
  const { id }: any = await prisma.tprp3.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function toprcData(documentID: string, token: any) {
  const { id }: any = await prisma.toprc.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}

export async function tprc1Data(documentID: string, token: any) {
  const { id }: any = await prisma.tprc1.findFirst({
    where: {
      docid: documentID ? documentID : triggerError,
      gtent_id: { docid: token },
    },
    select: { id: true },
  });

  return Number(id);
}
