import { MAX_PAGING, MAX_PAGING_GOODSTRACKER } from "../constant/paging";

// * Prisma Paging Variable
const maxPaging = MAX_PAGING;
const maxPagingGoodsTracker = MAX_PAGING_GOODSTRACKER;

export function skip(pointer: number, page_size?: number): number {
  return page_size ? page_size * (pointer - 1) : maxPaging * (pointer - 1);
}

export function defaultTake(page_size = 0): number {
  return page_size >= maxPaging
    ? Number(maxPaging)
    : Number(page_size ? page_size : maxPaging);
}

export function totalPage(counter: number, page_size: number): number {
  return Math.ceil(counter / page_size);
}

export function skipGoodsTracker(pointer: number, page_size?: number): number {
  return page_size
    ? page_size * (pointer - 1)
    : maxPagingGoodsTracker * (pointer - 1);
}

export function defaultTakeGoodsTracker(page_size = 0): number {
  return page_size >= maxPagingGoodsTracker
    ? Number(maxPagingGoodsTracker)
    : Number(page_size ? page_size : maxPagingGoodsTracker);
}
