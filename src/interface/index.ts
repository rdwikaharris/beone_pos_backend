import { JwtPayload } from "jsonwebtoken";

export interface ResultObject {
  statuscode: number;
  data: any;
}

export interface UserToken extends JwtPayload {
  email: string;
  gtentId: string;
  tousrId: string;
  torolId: string;
}
