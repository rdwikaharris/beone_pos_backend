import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { registerRule } from "../schema/express-validator/register";
import { usersQuery } from "../middleware";
import { prisma } from "../config/prisma";
import { gtentData } from "../utils/docid-id/global_lookup";
import { decodeToken } from "../utils/jwt";
import { hashPassword } from "../utils/bcrypt";
import {
  tohemData,
  torolData,
  tousrData,
} from "../utils/docid-id/tenant_lookup_1";
import { defaultTake, skip, totalPage } from "../config/variable";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";
import { param } from "express-validator";

const router = Router();
let resultObject: ResultObject;

// * GET USERS
router.get("/", usersQuery, async (req: Request, res: Response) => {
  const userToken: UserToken = await decodeToken(
    String(req.headers.authorization)
  );
  try {
    const {
      page,
      page_size,
      last_date,
      username,
      email,
      empcode,
      empname,
      rolecode,
      statusactive,
      superuser,
    }: any = req.query;

    const rows = await prisma.tousr.findMany({
      skip: skip(page, page_size),
      take: defaultTake(page_size),
      where: {
        updatedate: {
          gte: last_date,
        },
        gtentId: await gtentData(userToken.gtentId),
        username: {
          contains: username ? username : undefined,
        },
        email: {
          contains: email ? email : undefined,
        },
        // tohem_id: {
        //   empcode: {
        //     contains: empcode ? empcode : undefined,
        //   },
        //   empname: {
        //     contains: empname ? empname : undefined,
        //   },
        // },
        // torol_id: {
        //   rolecode: {
        //     contains: rolecode ? rolecode : undefined,
        //   },
        // },
        statusactive: statusactive ? parseInt(statusactive) : undefined,
        superuser: superuser ? parseInt(superuser) : undefined,
      },
      select: {
        docid: true,
        createdate: true,
        createby: true,
        updatedate: true,
        updateby: true,
        gtentId: true,
        // gtent_id: {
        //   select: {
        //     docid: true,
        //   },
        // },
        email: true,
        username: true,
        password: true,
        tohemId: true,
        // tohem_id: {
        //   select: {
        //     docid: true,
        //     empcode: true,
        //     empname: true,
        //   },
        // },
        torolId: true,
        // torol_id: {
        //   select: {
        //     docid: true,
        //     rolecode: true,
        //     rolename: true,
        //   },
        // },
        statusactive: true,
        activated: true,
        superuser: true,
        provider: true,
        usertype: true,
        trolleyuser: true,
      },
      orderBy: {
        id: "asc",
      },
    });

    if (page_size) {
      const [_counter] = await prisma.$transaction([
        prisma.tousr.count({
          where: {
            updatedate: {
              gte: last_date,
            },
            gtentId: await gtentData(userToken.gtentId),
            username: {
              contains: username ? username : undefined,
            },
            email: {
              contains: email ? email : undefined,
            },
            // tohem_id: {
            //   empcode: {
            //     contains: empcode ? empcode : undefined,
            //   },
            //   empname: {
            //     contains: empname ? empname : undefined,
            //   },
            // },
            torol_id: {
              rolecode: {
                contains: rolecode ? rolecode : undefined,
              },
            },
            statusactive: statusactive ? parseInt(statusactive) : undefined,
            superuser: superuser ? parseInt(superuser) : undefined,
          },
        }),
      ]);

      resultObject = {
        statuscode: 200,
        data: {
          rows: convertBigInt(rows),
          current_page: Number(page),
          total_page: totalPage(_counter, defaultTake(page_size)),
          total: _counter,
        },
      };
    }
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    console.error(err);
    resultObject = {
      statuscode: 500,
      data: err,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  }
});

// * GET USERS - BOS
router.get("/bos", async (req: Request, res: Response) => {
  const userToken: UserToken = await decodeToken(
    String(req.headers.authorization)
  );
  let token = null;

  const { password } = req.body;

  try {
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-user?page=1&page_size=20",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    console.error(err);
    resultObject = {
      statuscode: 500,
      data: err,
    };
    return res.status(resultObject.statuscode).json(resultObject);
  }
});

// * GET USER BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    const { docid } = req.params;
    try {
      const row = await prisma.tousr.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          email: true,
          username: true,
          tohem_id: {
            select: {
              docid: true,
              empcode: true,
              empname: true,
            },
          },
          torol_id: {
            select: {
              docid: true,
              rolecode: true,
              rolename: true,
            },
          },
          statusactive: true,
          activated: true,
          superuser: true,
          usertype: true,
          trolleyuser: true,
          trolleypass: true,
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      console.error(err);
      resultObject = {
        statuscode: 500,
        data: err,
      };
      return res.status(resultObject.statuscode).json(resultObject);
    }
  }
);

// * CREATE USER / REGISTER
router.post("/register", registerRule, async (req: Request, res: Response) => {
  const userToken: UserToken = await decodeToken(
    String(req.headers.authorization)
  );
  let _checkTrolleyUser: any = null;
  try {
    const {
      docid,
      createdate,
      createby,
      updatedate,
      updateby,
      email,
      username,
      password,
      tohem_id,
      torol_id,
      provider,
      trolleyuser,
      ...body
    } = req.body;

    const _checkEmail = await prisma.tousr.findFirst({
      where: { email },
      select: { email: true },
    });

    const _checkUserName = await prisma.tousr.findFirst({
      where: { username },
      select: { username: true },
    });

    if (trolleyuser) {
      _checkTrolleyUser = await prisma.tousr.findFirst({
        where: {
          trolleyuser,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          trolleyuser: true,
        },
      });
    }

    if (
      _checkEmail !== null ||
      _checkUserName !== null ||
      _checkTrolleyUser !== null
    ) {
      resultObject = {
        statuscode: 409,
        data: {
          description: "Duplicate",
          _checkEmail,
          _checkUserName,
          _checkTrolleyUser,
        },
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    }

    const _tousrData = await prisma.tousr.create({
      data: {
        createdate: new Date(),
        createby: await tousrData(userToken.tousrId, userToken.gtentId),
        updatedate: new Date(),
        updateby: await tousrData(userToken.tousrId, userToken.gtentId),
        gtentId: await gtentData(userToken.gtentId),
        tohemId: tohem_id
          ? await tohemData(tohem_id, userToken.gtentId)
          : undefined,
        torolId: await torolData(torol_id, userToken.gtentId),
        email,
        username,
        password: hashPassword(password),
        provider: 0,
        trolleyuser: trolleyuser ? trolleyuser : undefined,
        ...body,
      },
      select: {
        docid: true,
      },
    });

    resultObject = {
      statuscode: 200,
      data: {
        description: "Create User berhasil",
        docid: _tousrData.docid,
      },
    };
  } catch (err) {
    console.error(err);
    resultObject = {
      statuscode: 500,
      data: err,
    };
    return res.status(resultObject.statuscode).json(resultObject);
  }
});

export default router;

// router.post("/", registerRule, async (req: Request, res: Response) => {
//   try {
//     const err = validationResult(req);

//     let _checkTrolleyUser: any = null;

//     if (!err.isEmpty()) {
//       resultObject = {
//         statuscode: 400,
//         data: err.array(),
//       };
//       // console.log(err);
//       return res.status(resultObject.statuscode).json(resultObject.statuscode);
//     }

//     const {
//       createdate,
//       createby,
//       updatedate,
//       updateby,
//       lastchanges,
//       email,
//       username,
//       password,
//       statusactive,
//       activated,
//       superuser,
//       provider,
//       torol_id,
//       usertype,
//       trolleyuser,
//       gtent_id,
//       ...body
//     } = req.body;

//     const _checkEmail = await prisma.tousr.findFirst({
//       where: { email },
//       select: { email: true },
//     });

//     const _checkUsername = await prisma.tousr.findFirst({
//       where: { username },
//       select: { username: true },
//     });

//     if (trolleyuser) {
//       _checkTrolleyUser = await prisma.tousr.findFirst({
//         where: {
//           trolleyuser,
//         },
//         select: {
//           trolleyuser: true,
//         },
//       });
//     }

//     if (_checkEmail || _checkUsername || _checkTrolleyUser) {
//       resultObject = {
//         statuscode: 409,
//         data: {
//           description: "Duplicate",
//           _checkEmail,
//           _checkUsername,
//           _checkTrolleyUser,
//         },
//       };
//     }

//     const _tousrData = await prisma.tousr.create({
//       data: {
//         createdate: new Date(),
//         // ------
//         createby,
//         statusactive,
//         activated,
//         superuser,
//         // ------
//         // createby: await tousrData(userToken.tousrId, userToken.gtentId),
//         updatedate: new Date(),
//         // updateby: await tousrData(userToken.tousrId, userToken.gtentId),
//         // gtentId: await gtentData(userToken.gtentId),
//         // tohemId: tohem_id
//         //   ? await tohemData(tohem_id, userToken.gtentId)
//         //   : undefined,
//         // torolId: await torolData(torol_id, userToken.gtentId),
//         email,
//         username,
//         password: await hashPassword(password),
//         provider: 0,
//         trolleyuser: trolleyuser ? trolleyuser : undefined,
//         ...body,
//       },
//       select: {
//         docid: true,
//       },
//     });

//     resultObject = {
//       statuscode: 200,
//       data: {
//         description: "Create User Success",
//         docid: _tousrData.docid,
//       },
//     };

//     res.status(resultObject.statuscode).json(resultObject.data);
//   } catch (err) {
//     console.error(err);
//     resultObject = {
//       statuscode: 500,
//       data: err,
//     };
//     res.status(resultObject.statuscode).json(resultObject.data);
//   }
// });
