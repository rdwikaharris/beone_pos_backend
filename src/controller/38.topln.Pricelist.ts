import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import router from "./0.auth";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import { tostrData } from "../utils/docid-id/tenant_lookup_1";
import axios from "axios";
import fetchBosToken from "../utils/fetchToken";

const rouer = Router();
let resultObject: ResultObject;

// * GET PRICELISTS
router.get(
  "/",
  pageQuery,
  query("search_string").optional().isString(),
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const { page, page_size, last_date, search_string }: any = req.query;

      let _rowQueryComposite = {};

      if (search_string !== undefined) {
        _rowQueryComposite = {
          OR: [
            {
              pricecode: { contains: search_string },
            },
            {
              description: { contains: search_string },
            },
          ],
        };
      }

      const rows = await prisma.topln.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
          ..._rowQueryComposite,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          pricecode: true,
          description: true,
          baseprice: true,
          periodprice: true,
          factor: true,
          tcurr_id: {
            select: {
              docid: true,
              curcode: true,
              description: true,
            },
          },
          type: true,
          statusactive: true,
          activated: true,
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.topln.count({
            where: {
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
              ..._rowQueryComposite,
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET STORE PRICES
router.get(
  "/store-price",
  query("store_id").isUUID(),
  pageQuery,
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const { store_id, page, page_size, last_date }: any = req.query;

      const _storeData = await prisma.tostr.findFirst({
        where: {
          docid: store_id,
          gtentId: await gtentData(userToken.gtentId),
          statusactive: 1,
        },
        select: {
          toplnId: true,
        },
      });

      const rows = await prisma.tpln1.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          toplnId: _storeData?.toplnId,
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          topln_id: {
            select: {
              docid: true,
              pricecode: true,
              description: true,
            },
          },
          periodfr: true,
          periodto: true,
        },
        orderBy: {
          id: "asc",
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(rows),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET MEMBER PRICES
router.get(
  "/member-price",
  query("store_id").isUUID(),
  pageQuery,
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = decodeToken(
        String(req.headers.authorization)
      );
      let inStoreData: number[] = [];
      const { store_id, page, page_size, last_date }: any = req.query;

      const _storeData = await prisma.tpln3.findMany({
        where: {
          tostrId: await tostrData(store_id, userToken.gtentId),
          gtentId: await gtentData(userToken.gtentId),
          statusactive: 1,
        },
        select: {
          toplnId: true,
        },
      });

      // * Update converter
      for (let index = 0; index < _storeData.length; index++) {
        const element = _storeData[index];
        inStoreData.push(Number(element.toplnId));
      }

      const rows = await prisma.topln.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          type: 2,
          statusactive: 1,
          updatedate: {
            gte: last_date,
          },
          id: {
            in: inStoreData,
          },
        },
        select: {
          docid: true,
          pricecode: true,
          description: true,
          tpln1: {
            where: {
              updatedate: {
                gte: last_date,
              },
            },
            select: {
              docid: true,
              periodfr: true,
              periodto: true,
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(rows),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET PRICELISTS - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-pricelist/?page=1&page_size=10",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET STORE PRICES - BOS
router.get("/bos/store-price", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-pricelist/?store_id=276e5b7f-10ff-4cef-9505-68e59f1c2c00&page=1",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET MEMBER PRICES - BOS
router.get("/bos/member-price", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-pricelist/member-price/?store_id=7b2a452f-f506-4a75-b6ad-78f969fab261&page=1&last_date=2022-10-11T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET PRICELIST BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-pricelist/docid/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET PRICELIST BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.topln.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          pricecode: true,
          description: true,
          baseprice: true,
          periodprice: true,
          factor: true,
          tcurr_id: {
            select: {
              docid: true,
            },
          },
          type: true,
          statusactive: true,
          activated: true,
          tpln1: {
            select: {
              docid: true,
              periodfr: true,
              periodto: true,
              baseprice: true,
              periodprice: true,
              factor: true,
              statusactive: true,
              activated: true,
            },
          },
          tpln3: {
            select: {
              docid: true,
              tostr_id: {
                select: {
                  docid: true,
                },
              },
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
