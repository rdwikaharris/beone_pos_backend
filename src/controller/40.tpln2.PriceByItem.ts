import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { userState } from "../functions/helper";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { tpln1Data } from "../utils/docid-id/tenant_lookup_1";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET PRICES BY ITEM
router.get(
  "/",
  query("price_period_id").isUUID(),
  pageQuery,
  query("search_item").optional().isString(),
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = decodeToken(
        String(req.headers.authorization)
      );
      const currentState = await userState(userToken);
      const { price_period_id, page, page_size, last_date, search_item }: any =
        req.query;

      const rows = await prisma.tpln2.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          toitm_id: {
            itemname: {
              contains: search_item,
            },
          },
          tpln1Id: await tpln1Data(price_period_id, userToken.gtentId),
          updatedate: {
            gte: last_date,
          },
          gtentId: currentState.tenant,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          tpln1_id: {
            select: {
              docid: true,
              periodfr: true,
              periodto: true,
            },
          },
          toitm_id: {
            select: {
              docid: true,
              itemcode: true,
              itemname: true,
            },
          },
          tcurr_id: {
            select: {
              docid: true,
              curcode: true,
            },
          },
          price: true,
          // purchaseprice: true,
          // calculatedprice: true,
          // marginvalue: true,
          // marginpercentage: true,
          // costprice: true,
          // beforerounding: true,
          // afterrounding: true,
          // roundingdiff: true,
          tpln4: {
            select: {
              docid: true,
              tbitm_id: {
                select: {
                  docid: true,
                  barcode: true,
                  statusactive: true,
                },
              },
              // tcurr_id: {
              //   select: {
              //     docid: true,
              //     curcode: true,
              //     description: true,
              //   },
              // },
              price: true,
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tpln2.count({
            where: {
              toitm_id: {
                itemname: {
                  contains: search_item,
                },
              },
              tpln1Id: await tpln1Data(price_period_id, userToken.gtentId),
              updatedate: {
                gte: last_date,
              },
              gtentId: currentState.tenant,
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET PRICES BY ITEM - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-price-by-item/?price_period_id=541be024-b49d-4048-af0a-e4d4683f909b&page=1&page_size=3&last_date=2023-10-05T06:00:07.000Z&search_item=SESA Rosemary Grilled",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET PRICES BY ITEM BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-price-by-item/all/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET PRICES BY ITEM BY ID
router.get(
  "/all/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;

    try {
      const userToken: UserToken = decodeToken(
        String(req.headers.authorization)
      );
      const currentState = await userState(userToken);

      const row = await prisma.tpln2.findMany({
        where: {
          tpln1Id: await tpln1Data(docid, userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          tpln1_id: {
            select: {
              docid: true,
              periodfr: true,
              periodto: true,
              baseprice: true,
              periodprice: true,
              factor: true,
              statusactive: true,
              activated: true,
            },
          },
          toitm_id: {
            select: {
              docid: true,
              itemcode: true,
              itemname: true,
              invitem: true,
              serialno: true,
              minstock: true,
              maxstock: true,
              includetax: true,
              remarks: true,
              statusactive: true,
              activated: true,
            },
          },
          tcurr_id: {
            select: {
              docid: true,
              curcode: true,
            },
          },
          price: true,
          // purchaseprice: true,
          // calculatedprice: true,
          // marginvalue: true,
          // marginpercentage: true,
          // costprice: true,
          // beforerounding: true,
          // afterrounding: true,
          // roundingdiff: true,
          tpln4: {
            select: {
              docid: true,
              tbitm_id: {
                select: {
                  docid: true,
                  barcode: true,
                  statusactive: true,
                },
              },
              // tcurr_id: {
              //   select: {
              //     docid: true,
              //     curcode: true,
              //     description: true,
              //   },
              // },
              price: true,
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
