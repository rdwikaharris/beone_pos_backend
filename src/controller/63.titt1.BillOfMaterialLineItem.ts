import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { toittData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET BILL OF MATERIAL LINE ITEM
router.get(
  "/",
  query("toitt_id").isUUID(),
  pageQuery,
  // authRoleMiddleware("f1041", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const { toitt_id, page, page_size, last_date }: any = req.query;

      const rows = await prisma.titt1.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          toittId: await toittData(toitt_id, userToken.gtentId),
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          // tcurr_id: {
          //   select: {
          //     docid: true,
          //     curcode: true,
          //     description: true,
          //   },
          // },
          // price: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          toitm_id: {
            select: {
              docid: true,
              itemcode: true,
              itemname: true,
            },
          },
          quantity: true,
          touom_id: {
            select: {
              docid: true,
              uomcode: true,
              uomdesc: true,
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.titt1.count({
            where: {
              toittId: await toittData(toitt_id, userToken.gtentId),
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET BILL OF MATERIAL LINE ITEM - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-bill-of-material-line-item?page=1&page_size=10&last_date=2021-10-06T00:00:00.000Z&toitt_id=a6d85e50-5df8-4da5-95a1-67530ba24af9",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET BILL OF MATERIAL LINE ITEM BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-bill-of-material-line-item/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET BILL OF MATERIAL LINE ITEM BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1041", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.titt1.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          // tcurr_id: {
          //   select: {
          //     docid: true,
          //     curcode: true,
          //     description: true,
          //   },
          // },
          // price: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          toitm_id: {
            select: {
              docid: true,
              itemcode: true,
              itemname: true,
            },
          },
          quantity: true,
          touom_id: {
            select: {
              docid: true,
              uomcode: true,
              uomdesc: true,
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
