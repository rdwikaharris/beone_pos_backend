import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { tocatData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { ItemHierarchy } from "../functions/itemHierarchy";
import { convertBigInt } from "../utils/jsonbigint";
import { userState } from "../functions/helper";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET ITEM MASTERS
router.get(
  "/",
  pageQuery,
  query("tocat_id").optional().isUUID(),
  query("itemname").optional().isString(),
  query("search_string").optional().isString(),
  query("statusactive").optional().isIn([0, 1]),
  // authRoleMiddleware("f1029", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const {
        page,
        page_size,
        last_date,
        tocat_id,
        itemname,
        statusactive,
        search_string,
      }: any = req.query;

      const rows = await prisma.toitm.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          OR: [
            { itemname: { contains: search_string } },
            { itemcode: { contains: search_string } },
            // { touom_id: { uomdesc: { contains: search_string } } },
            // { remarks: { contains: search_string } },
            // {
            //   tocat_id: {
            //     OR: [
            //       { catcode: { contains: search_string } },
            //       { catname: { contains: search_string } },
            //       {
            //         parent_id: {
            //           OR: [
            //             { catcode: { contains: search_string } },
            //             { catname: { contains: search_string } },
            //           ],
            //         },
            //       },
            //     ],
            //   },
            // },
          ],
          tocatId: tocat_id
            ? await tocatData(tocat_id, userToken.gtentId)
            : undefined,
          updatedate: {
            gte: last_date,
          },
          statusactive: statusactive ? JSON.parse(statusactive) : undefined,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          itemcode: true,
          itemname: true,
          invitem: true,
          serialno: true,
          tocat_id: {
            select: {
              docid: true,
              catcode: true,
              catname: true,
              parent_id: {
                select: {
                  docid: true,
                  catcode: true,
                  catname: true,
                },
              },
            },
          },
          touom_id: {
            select: {
              docid: true,
              uomcode: true,
              uomdesc: true,
              statusactive: true,
              activated: true,
            },
          },
          minstock: true,
          maxstock: true,
          includetax: true,
          remarks: true,
          statusactive: true,
          activated: true,
          isbatch: true,
          internalcode_1: true,
          internalcode_2: true,
          openprice: true,
          // popitem: true,
          // bpom: true,
          // expdate: true,
          // multiplyorder: true,
          // margin: true,
          // memberdiscount: true,
          // mergequantity: true,
          tbitm: {
            take: 1,
            select: {
              barcode: true,
            },
            // orderBy: {
            //   dflt: "desc",
            // },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      // * Item Hierarchy assigning loop
      for (const row in rows) {
        if (
          rows[row].tocat_id?.catcode === null ||
          rows[row].tocat_id?.catcode === undefined
        ) {
          continue;
        }

        const _itemHierarchy: object = {
          itemHierarchy: await ItemHierarchy(
            rows[row].tocat_id?.catcode,
            await gtentData(userToken.gtentId)
          ),
        };
        rows[row] = {
          ...rows[row],
          ..._itemHierarchy,
        };
      }

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.toitm.count({
            where: {
              OR: [
                { itemname: { contains: search_string } },
                { itemcode: { contains: search_string } },
                // { touom_id: { uomdesc: { contains: search_string } } },
                // { remarks: { contains: search_string } },
                // {
                //   tocat_id: {
                //     OR: [
                //       { catcode: { contains: search_string } },
                //       { catname: { contains: search_string } },
                //       {
                //         parent_id: {
                //           OR: [
                //             { catcode: { contains: search_string } },
                //             { catname: { contains: search_string } },
                //           ],
                //         },
                //       },
                //     ],
                //   },
                // },
              ],
              tocatId: tocat_id
                ? await tocatData(tocat_id, userToken.gtentId)
                : undefined,
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET ITEM MASTERS HIERARCHY
router.get(
  "/hierarchy",
  pageQuery,
  query("statusactive").optional().isIn([0, 1]),
  query("itemcode")
    .optional()
    .isString()
    .customSanitizer((value) => (value.trim() === "" ? undefined : value)),
  query("itemname")
    .optional()
    .isString()
    .customSanitizer((value) => (value.trim() === "" ? undefined : value)),
  query("uom")
    .optional()
    .isString()
    .customSanitizer((value) => (value.trim() === "" ? undefined : value)),
  query("remarks")
    .optional()
    .isString()
    .customSanitizer((value) => (value.trim() === "" ? undefined : value)),
  query("search_string")
    .optional()
    .isString()
    .customSanitizer((value) => (value.trim() === "" ? undefined : value)),
  //  * For the hierarchy, not protected
  // authRoleMiddleware("f1029", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const currentState = await userState(userToken);
      const {
        page,
        page_size,
        last_date,
        itemcode,
        itemname,
        uom,
        remarks,
        statusactive,
        search_string,
        ...query
      }: any = req.query;
      const queryHeaderList: any[] = [];
      const queryValueList: any[] = [];
      let rows: any[] = [];
      let catcodeList: any[] | undefined;
      let _rowsQueryComposite: any = {};

      if (Object.keys(query).length > 0) {
        for (const item in query) {
          if (query[item] === "") {
            // * Nothing to filter
          } else {
            queryHeaderList.push(item);
            queryValueList.push(query[item]);
          }
        }

        if (queryHeaderList.length > 0) {
          const hierarchyStructure: any[] = await prisma.tphir.findMany({
            where: {
              description: {
                in: queryHeaderList,
              },
              gtentId: await gtentData(userToken.gtentId),
            },
            select: {
              docid: true,
              description: true,
              level: true,
              maxchar: true,
            },
            orderBy: {
              level: "asc",
            },
          });

          if (hierarchyStructure.length === 0) {
            resultObject = {
              statuscode: 500,
              data: {
                description: "No hierarchy level found based on input",
              },
            };

            return res.status(resultObject.statuscode).json(resultObject.data);
          }

          let _queryComposite: any = {};
          let _counter = 0;
          const _lowestHierarchyLevel: number =
            hierarchyStructure[hierarchyStructure.length - 1].level;
          const _highestHierarchyLevel: number = hierarchyStructure[0].level;

          for (
            let i = _highestHierarchyLevel;
            i < _lowestHierarchyLevel + 1;
            i++
          ) {
            if (hierarchyStructure[_counter].level !== i) {
              // * Gap, tidak ada data yang direturn dari DB
              _queryComposite = { parent_id: { ..._queryComposite } };
              continue;
            }

            for (let j = 0; j < queryHeaderList.length; j++) {
              if (
                hierarchyStructure[_counter].description.toLowerCase() ===
                queryHeaderList[_counter].toLowerCase()
              ) {
                _queryComposite = {
                  parent_id: { ..._queryComposite },
                  phlir1_id: {
                    OR: [
                      { code: { contains: queryValueList[_counter] } },
                      { description: { contains: queryValueList[_counter] } },
                    ],
                    tphir_id: {
                      description: { contains: queryHeaderList[_counter] },
                    },
                  },
                };

                _counter++;
                break;
              }
            }
          }

          const categoryList: any[] = await prisma.tocat.findMany({
            where: {
              ..._queryComposite,
              gtent_id: gtentData(userToken.gtentId),
            },
            select: {
              docid: true,
              catcode: true,
              catname: true,
              level: true,
              parent_id: {
                select: {
                  docid: true,
                  catcode: true,
                  catname: true,
                  level: true,
                },
              },
              phlir1_id: {
                select: {
                  description: true,
                  tphir_id: {
                    select: {
                      description: true,
                    },
                  },
                },
              },
            },
          });

          catcodeList = categoryList.map(({ catcode }) => {
            return { catcode: { startsWith: catcode } };
          });
        }
      }

      // Final filtering query composite
      if (itemcode !== undefined || itemname !== undefined) {
        _rowsQueryComposite = {
          ..._rowsQueryComposite,
          itemcode: { contains: itemcode },
          itemname: { contains: itemname },
        };
      }

      if (uom !== undefined) {
        _rowsQueryComposite = {
          ..._rowsQueryComposite,
          OR: [
            { touom_id: { uomcode: { contains: uom } } },
            { touom_id: { uomdesc: { contains: uom } } },
          ],
        };
      }

      if (catcodeList !== undefined) {
        _rowsQueryComposite = {
          ..._rowsQueryComposite,
          tocat_id: {
            OR: catcodeList,
          },
        };
      }

      _rowsQueryComposite = {
        ..._rowsQueryComposite,
        remarks,
        updatedate: { gte: last_date },
        statusactive: statusactive ? JSON.parse(statusactive) : undefined,
        gtentId: await gtentData(userToken.gtentId),
      };

      rows = await prisma.toitm.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: _rowsQueryComposite,
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          itemcode: true,
          itemname: true,
          invitem: true,
          serialno: true,
          tocat_id: {
            select: {
              docid: true,
              catcode: true,
              catname: true,
              parent_id: {
                select: {
                  docid: true,
                  catcode: true,
                  catname: true,
                },
              },
            },
          },
          touom_id: {
            select: {
              docid: true,
              uomcode: true,
              uomdesc: true,
              statusactive: true,
              activated: true,
            },
          },
          minstock: true,
          maxstock: true,
          includetax: true,
          remarks: true,
          statusactive: true,
          activated: true,
          isbatch: true,
          internalcode_1: true,
          internalcode_2: true,
          openprice: true,
          // popitem: true,
          // bpom: true,
          // expdate: true,
          // multiplyorder: true,
          // margin: true,
          // memberdiscount: true,
          // mergequantity: true,
          tbitm: {
            take: 1,
            select: {
              barcode: true,
            },
            // orderBy: {
            //   dflt: "desc",
            // },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      // * Item Hierarchy assigning loop
      for (const row in rows) {
        if (
          rows[row].tocat_id?.catcode === null ||
          rows[row].tocat_id?.catcode === undefined
        ) {
          continue;
        }

        const _itemHierarchy: object = {
          itemHierarchy: await ItemHierarchy(
            rows[row].tocat_id?.catcode,
            await gtentData(userToken.gtentId)
          ),
        };

        rows[row] = {
          ...rows[row],
          ..._itemHierarchy,
        };
      }

      if (page_size) {
        const _counter = await prisma.toitm.count({
          where: _rowsQueryComposite,
        });

        return res.status(200).json({
          rows: convertBigInt(rows),
          current_page: Number(page),
          total_page: totalPage(_counter, defaultTake(page_size)),
          total: _counter,
        });
      } else {
        return res.status(200).json(convertBigInt(rows));
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET ITEM MASTERS HIERARCHY - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;
  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-item-master/hierarchy?page=1&page_size=10",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET ITEM MASTER BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-item-master/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET ITEM MASTER BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1029", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      let row: any = await prisma.toitm.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          itemcode: true,
          itemname: true,
          invitem: true,
          serialno: true,
          tocat_id: {
            select: {
              docid: true,
              catcode: true,
              catname: true,
            },
          },
          touom_id: {
            select: {
              docid: true,
              uomcode: true,
              uomdesc: true,
              statusactive: true,
              activated: true,
            },
          },
          minstock: true,
          maxstock: true,
          includetax: true,
          remarks: true,
          statusactive: true,
          activated: true,
          isbatch: true,
          internalcode_1: true,
          internalcode_2: true,
          openprice: true,
          // popitem: true,
          // bpom: true,
          // expdate: true,
          // multiplyorder: true,
          // margin: true,
          // memberdiscount: true,
          // mergequantity: true,
          property_1: {
            select: {
              docid: true,
              properties: true,
              code: true,
              description: true,
            },
          },
          property_2: {
            select: {
              docid: true,
              properties: true,
              code: true,
              description: true,
            },
          },
          property_3: {
            select: {
              docid: true,
              properties: true,
              code: true,
              description: true,
            },
          },
          property_4: {
            select: {
              docid: true,
              properties: true,
              code: true,
              description: true,
            },
          },
          property_5: {
            select: {
              docid: true,
              properties: true,
              code: true,
              description: true,
            },
          },
          property_6: {
            select: {
              docid: true,
              properties: true,
              code: true,
              description: true,
            },
          },
          property_7: {
            select: {
              docid: true,
              properties: true,
              code: true,
              description: true,
            },
          },
          property_8: {
            select: {
              docid: true,
              properties: true,
              code: true,
              description: true,
            },
          },
          property_9: {
            select: {
              docid: true,
              properties: true,
              code: true,
              description: true,
            },
          },
          property_10: {
            select: {
              docid: true,
              properties: true,
              code: true,
              description: true,
            },
          },
          tbitm: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              barcode: true,
              statusactive: true,
              activated: true,
              quantity: true,
              touom_id: {
                select: {
                  docid: true,
                  uomcode: true,
                  uomdesc: true,
                },
              },
            },
          },
          tsitm: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              tostr_id: {
                select: {
                  docid: true,
                  storecode: true,
                  storename: true,
                },
              },
              statusactive: true,
              activated: true,
            },
          },
          tpitm: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              picture: true,
            },
          },
          tritm: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              remarks: true,
            },
          },
        },
      });

      // Item Hierarchy assigning
      if (row !== null || row !== undefined) {
        const _itemHierarchy: object = {
          itemHierarchy: await ItemHierarchy(
            row?.tocat_id?.catcode,
            await gtentData(userToken.gtentId)
          ),
        };
        row = {
          ...row,
          ..._itemHierarchy,
        };
      }

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
