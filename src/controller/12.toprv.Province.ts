import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { authRoleMiddleware, pageQuery } from "../middleware";
import { param, query } from "express-validator";
// import { CAN_READ } from "../constant/paging";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { tocryData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import { defaultTake, skip, totalPage } from "../config/variable";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET PROVINCES
router.get(
  "/",
  pageQuery,
  query("tocry_id").optional().isUUID(),
  query("search_string").optional().isString(),
  query(["provnccode", "description", "descriptionfrgn", "countrycode"])
    .optional()
    .isString(),
  // authRoleMiddleware("f1018", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const {
        tocry_id,
        page,
        page_size,
        last_date,
        search_string,
        provnccode,
        description,
        descriptionfrgn,
        countrycode,
      }: any = req.query;

      let _rowQueryComposite = {};

      if (search_string !== undefined) {
        _rowQueryComposite = {
          OR: [
            {
              provnccode: { contains: search_string },
            },
            {
              description: { contains: search_string },
            },
            {
              descriptionfrgn: { contains: search_string },
            },
          ],
        };
      }

      const rows = await prisma.toprv.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          tocryId:
            tocry_id !== undefined
              ? await tocryData(tocry_id, userToken.gtentId)
              : undefined,
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
          ..._rowQueryComposite,
          provnccode,
          description: {
            contains: description ? description : undefined,
          },
          descriptionfrgn: {
            contains: descriptionfrgn ? descriptionfrgn : undefined,
          },
          tocry_id: {
            countrycode,
          },
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          provnccode: true,
          description: true,
          descriptionfrgn: true,
          tocry_id: {
            select: {
              docid: true,
              countrycode: true,
              description: true,
              descriptionfrgn: true,
              phonecode: true,
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.toprv.count({
            where: {
              tocryId:
                tocry_id !== undefined
                  ? await tocryData(tocry_id, userToken.gtentId)
                  : undefined,
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
              ..._rowQueryComposite,
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET PROVINCES - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/global-master-province?page=1&page_size=5&last_date=2021-01-01T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET PROVINCE BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/global-master-province/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET PROVINCES BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1017", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const row = await prisma.toprv.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          provnccode: true,
          description: true,
          descriptionfrgn: true,
          tocry_id: {
            select: {
              docid: true,
              countrycode: true,
              description: true,
              descriptionfrgn: true,
              phonecode: true,
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
