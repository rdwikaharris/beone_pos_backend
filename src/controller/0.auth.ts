import { Request, Response, Router } from "express";
import { ResultObject } from "../interface";
import { checkSchema } from "express-validator";
import { LoginAuthSchema } from "../schema/express-validator/login";
import { validatePage } from "../middleware";
import { prisma } from "../config/prisma";
import { verivyPassword } from "../utils/bcrypt";
import { tokenJWT } from "../utils/jwt";
import md5 from "md5";

const router = Router();
let resultObject: ResultObject;

// * LOGIN
router.post(
  "/login",
  validatePage,
  checkSchema(LoginAuthSchema),
  async (req: Request, res: Response) => {
    try {
      const { identifier, password } = req.body;

      const getUserData = await prisma.tousr.findFirst({
        where: {
          OR: [{ username: identifier }, { email: identifier }],
        },
        select: {
          docid: true,
          username: true,
          password: true,
          email: true,
          gtent_id: {
            select: {
              docid: true,
              compname: true,
            },
          },
          statusactive: true,
          torol_id: {
            select: {
              docid: true,
            },
          },
        },
      });

      if (
        !getUserData ||
        getUserData?.statusactive === 0 ||
        md5(password) !== getUserData?.password
      ) {
        resultObject = {
          statuscode: 400,
          data: {
            description: "Wrong Usernmae/Email/Password or User not Active",
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }

      const token = tokenJWT({
        email: getUserData.email,
        gtentId: getUserData.gtent_id?.docid,
        tousrId: getUserData.docid,
        torolId: getUserData.torol_id?.docid,
      });

      resultObject = {
        statuscode: 200,
        data: {
          description: "You have Successfully Login",
          compname: getUserData.gtent_id?.compname,
          token,
        },
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      console.error(err);
      resultObject = {
        statuscode: 500,
        data: {
          description: "Login Failed",
          data: { err },
        },
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    }
  }
);

export default router;
