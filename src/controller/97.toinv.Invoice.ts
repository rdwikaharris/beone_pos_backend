import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { tostrData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET INVOICE
router.get(
  "/",
  pageQuery,
  query("search_docnum").optional().isString(),
  query("tostr_id").optional().isUUID(),
  // authRoleMiddleware("f1056", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const { page, page_size, last_date, search_docnum, tostr_id }: any =
        req.query;

      const rows = await prisma.toinv.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          updatedate: {
            gte: last_date,
          },
          docnum: {
            contains: search_docnum,
          },
          tostrId: tostr_id
            ? await tostrData(tostr_id, userToken.gtentId)
            : undefined,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          tostr_id: {
            select: {
              docid: true,
              storecode: true,
              storename: true,
            },
          },
          docnum: true,
          orderno: true,
          tocus_id: {
            select: {
              docid: true,
              custcode: true,
              custname: true,
              isemployee: true,
            },
          },
          tohem_id: {
            select: {
              docid: true,
              empcode: true,
              empname: true,
            },
          },
          transdate: true,
          transtime: true,
          timezone: true,
          remarks: true,
          subtotal: true,
          discprctg: true,
          discamount: true,
          discountcard: true,
          coupon: true,
          discountcoupon: true,
          taxprctg: true,
          taxamount: true,
          addcost: true,
          rounding: true,
          grandtotal: true,
          changed: true,
          totalpayment: true,
          tocsr_id: {
            select: {
              docid: true,
              description: true,
            },
          },
          toinv_tohem_id: {
            select: {
              docid: true,
              empcode: true,
              empname: true,
            },
          },
          tinv1: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              linenum: true,
              docnum: true,
              idnumber: true,
              toitm_id: {
                select: {
                  docid: true,
                  itemcode: true,
                  itemname: true,
                },
              },
              quantity: true,
              sellingprice: true,
              discprctg: true,
              discamount: true,
              totalamount: true,
              taxprctg: true,
              promotiontype: true,
              promotionid: true,
              remarks: true,
              edittime: true,
              cogs: true,
              tovat_id: {
                select: {
                  docid: true,
                  taxcode: true,
                  description: true,
                },
              },
              promotiontingkat: true,
              promovoucherno: true,
              basedocid: true,
              baselinedocid: true,
              includetax: true,
              // toven_id: {
              //   select: {
              //     docid: true,
              //     vendcode: true,
              //     vendname: true,
              //   },
              // },
              tbitm_id: {
                select: {
                  docid: true,
                  barcode: true,
                  touom_id: {
                    select: {
                      docid: true,
                      uomcode: true,
                      uomdesc: true,
                    },
                  },
                },
              },
              // qtybarcode: true,
              // sellpricebarcode: true,
              // totalsellbarcode: true,
              // disc1pct: true,
              // disc1amt: true,
              // disc2pct: true,
              // disc2amt: true,
              // disc3pct: true,
              // disc3amt: true,
              // disc1pctbarcode: true,
              // disc1amtbarcode: true,
              // disc2pctbarcode: true,
              // disc2amtbarcode: true,
              // disc3pctbarcode: true,
              // disc3amtbarcode: true,
              // totaldiscbarcode: true,
              // qtyconv: true,
              // ppobmargin: true,
              // grosspriceppob: true,
              // discprctgmember: true,
              // discamountmember: true,
            },
          },
          tinv2: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              linenum: true,
              tpmt3_id: {
                select: {
                  docid: true,
                  tpmt1_id: {
                    select: {
                      docid: true,
                      mopcode: true,
                      description: true,
                      mopalias: true,
                      bankcharge: true,
                      validforemp: true,
                    },
                  },
                  tostr_id: {
                    select: {
                      docid: true,
                      storecode: true,
                      storename: true,
                    },
                  },
                },
              },
              amount: true,
              tpmt2_id: {
                select: {
                  docid: true,
                  cccode: true,
                  description: true,
                  cardtype: true,
                },
              },
              cardno: true,
              cardholder: true,
              sisavoucher: true,
            },
          },
          tinv4: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              type: true,
              serialno: true,
              amount: true,
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.toinv.count({
            where: {
              updatedate: {
                gte: last_date,
              },
              docnum: {
                contains: search_docnum,
              },
              tostrId: tostr_id
                ? await tostrData(tostr_id, userToken.gtentId)
                : undefined,
              gtentId: await gtentData(userToken.gtentId),
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET INVOICE - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-invoice?page=1&page_size=1&last_date=2023-01-01T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET INVOICE BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-invoice/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET INVOICE BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1056", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.toinv.findFirst({
        where: {
          docid: docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          tostr_id: {
            select: {
              docid: true,
              storecode: true,
              storename: true,
            },
          },
          docnum: true,
          orderno: true,
          tocus_id: {
            select: {
              docid: true,
              custcode: true,
              custname: true,
              isemployee: true,
            },
          },
          tohem_id: {
            select: {
              docid: true,
              empcode: true,
              empname: true,
            },
          },
          transdate: true,
          transtime: true,
          timezone: true,
          remarks: true,
          subtotal: true,
          discprctg: true,
          discamount: true,
          discountcard: true,
          coupon: true,
          discountcoupon: true,
          taxprctg: true,
          taxamount: true,
          addcost: true,
          rounding: true,
          grandtotal: true,
          changed: true,
          totalpayment: true,
          tocsr_id: {
            select: {
              docid: true,
              description: true,
            },
          },
          toinv_tohem_id: {
            select: {
              docid: true,
              empcode: true,
              empname: true,
            },
          },
          tinv1: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              linenum: true,
              docnum: true,
              idnumber: true,
              toitm_id: {
                select: {
                  docid: true,
                  itemcode: true,
                  itemname: true,
                },
              },
              quantity: true,
              sellingprice: true,
              discprctg: true,
              discamount: true,
              totalamount: true,
              taxprctg: true,
              promotiontype: true,
              promotionid: true,
              remarks: true,
              edittime: true,
              cogs: true,
              tovat_id: {
                select: {
                  docid: true,
                  taxcode: true,
                  description: true,
                },
              },
              promotiontingkat: true,
              promovoucherno: true,
              basedocid: true,
              baselinedocid: true,
              includetax: true,
              // toven_id: {
              //   select: {
              //     docid: true,
              //     vendcode: true,
              //     vendname: true,
              //   },
              // },
              tbitm_id: {
                select: {
                  docid: true,
                  barcode: true,
                  quantity: true,
                  touom_id: {
                    select: {
                      docid: true,
                      uomcode: true,
                      uomdesc: true,
                    },
                  },
                },
              },
              // qtybarcode: true,
              // sellpricebarcode: true,
              // totalsellbarcode: true,
              // disc1pct: true,
              // disc1amt: true,
              // disc2pct: true,
              // disc2amt: true,
              // disc3pct: true,
              // disc3amt: true,
              // disc1pctbarcode: true,
              // disc1amtbarcode: true,
              // disc2pctbarcode: true,
              // disc2amtbarcode: true,
              // disc3pctbarcode: true,
              // disc3amtbarcode: true,
              // totaldiscbarcode: true,
              // qtyconv: true,
              // ppobmargin: true,
              // grosspriceppob: true,
              // discprctgmember: true,
              // discamountmember: true,
            },
          },
          tinv2: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              linenum: true,
              tpmt3_id: {
                select: {
                  docid: true,
                  tpmt1_id: {
                    select: {
                      docid: true,
                      mopcode: true,
                      description: true,
                      mopalias: true,
                      bankcharge: true,
                      validforemp: true,
                    },
                  },
                  tostr_id: {
                    select: {
                      docid: true,
                      storecode: true,
                      storename: true,
                    },
                  },
                },
              },
              amount: true,
              tpmt2_id: {
                select: {
                  docid: true,
                  cccode: true,
                  description: true,
                  cardtype: true,
                },
              },
              cardno: true,
              cardholder: true,
              sisavoucher: true,
            },
          },
          tinv4: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              type: true,
              serialno: true,
              amount: true,
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
