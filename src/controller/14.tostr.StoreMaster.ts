import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { defaultTake, skip, totalPage } from "../config/variable";
import { prisma } from "../config/prisma";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET STORES
router.get(
  "/",
  pageQuery,
  query("search_string").optional().isString(),
  query(["storename", "storecode"]).optional().isString(),
  query("statusactive").optional().isIn([1, 0]),
  // authRoleMiddleware("f1020", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const {
        page,
        page_size,
        last_date,
        search_string,
        storename,
        storecode,
        statusactive,
      }: any = req.query;

      let _rowQueryComposite = {};

      if (search_string !== undefined) {
        _rowQueryComposite = {
          OR: [
            {
              storecode: { contains: search_string },
            },
            {
              storename: { contains: search_string },
            },
          ],
        };
      }

      const rows = await prisma.tostr.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
          storecode,
          storename: {
            contains: storename ? storename : undefined,
          },
          statusactive,
          ..._rowQueryComposite,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          storecode: true,
          storename: true,
          email: true,
          phone: true,
          addr1: true,
          addr2: true,
          addr3: true,
          city: true,
          remarks: true,
          toprv_id: {
            select: {
              docid: true,
              provnccode: true,
              description: true,
            },
          },
          tocry_id: {
            select: {
              docid: true,
              countrycode: true,
              description: true,
            },
          },
          tozcd_id: {
            select: {
              docid: true,
              zipcode: true,
              city: true,
              district: true,
              urban: true,
              subdistrict: true,
            },
          },
          tohem_id: {
            select: {
              docid: true,
              empcode: true,
              empname: true,
            },
          },
          sqm: true,
          tcurr_id: {
            select: {
              docid: true,
              curcode: true,
              description: true,
            },
          },
          topln_id: {
            select: {
              docid: true,
              pricecode: true,
              description: true,
              tcurr_id: {
                select: {
                  curcode: true,
                  description: true,
                },
              },
            },
          },
          storepic: true,
          tovat_id: {
            select: {
              docid: true,
              taxcode: true,
              rate: true,
            },
          },
          storeopen: true,
          statusactive: true,
          activated: true,
          tocsr: {
            select: {
              hwkey: true,
              token: true,
              email: true,
              statusactive: true,
              activated: true,
            },
          },
          prefixdoc: true,
          header01: true,
          header02: true,
          header03: true,
          header04: true,
          header05: true,
          footer01: true,
          footer02: true,
          footer03: true,
          footer04: true,
          footer05: true,
          sellingtax: true,
          openingbalance: true,
          // autorounding: true,
          // roundingvalue: true,
          // totalminus: true,
          // totalzero: true,
          // holdstruck: true,
          // holdclose: true,
          // autoprintstruk: true,
          // barcode1: true,
          // barcode2: true,
          // barcode3: true,
          // barcode4: true,
          // connectback: true,
          // maxuserkassa: true,
          // stocklevel: true,
          // minconst: true,
          // maxconst: true,
          // ordercycle: true,
          // taxby: true,
          // mtxline01: true,
          // mtxline02: true,
          // mtxline03: true,
          // mtxline04: true,
          // storepicpath: true,
          // autosync: true,
          // autodownload: true,
          // autodownload1: true,
          // autodownload2: true,
          // autodownload3: true,
          // autoupload: true,
          // vdfline1: true,
          // vdfline2: true,
          // vdfline1off: true,
          // vdfline2off: true,
          // searchitem: true,
          // roundingremarks: true,
          // checksellingprice: true,
          // qtyminusvalidation: true,
          // checkstockminus: true,
          // maxvoiddays: true,
          // attendacefp: true,
          // credittaxcode_id: {
          //   select: {
          //     docid: true,
          //     taxcode: true,
          //     rate: true,
          //   },
          // },
        },
        orderBy: {
          id: "asc",
        },
      });

      // const newRows = rows.map(
      //   ({ autodownload1, autodownload2, autodownload3, ...data }) => {
      //     return {
      //       ...data,
      //       autodownload1,
      //       autodownload2,
      //       autodownload3,
      //       timeautodownload1: moment.utc(autodownload1).format("HH:mm:ss"),
      //       timeautodownload2: moment.utc(autodownload2).format("HH:mm:ss"),
      //       timeautodownload3: moment.utc(autodownload3).format("HH:mm:ss"),
      //     };
      //   }
      // );

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tostr.count({
            where: {
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
              ..._rowQueryComposite,
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET STORES - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-store-master?page=1&page_size=20&last_date=2021-10-06T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET STORE BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-store-master/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError;
    }
  }
);

// * GET STORE BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1017", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.tostr.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          storecode: true,
          storename: true,
          email: true,
          phone: true,
          addr1: true,
          addr2: true,
          addr3: true,
          city: true,
          remarks: true,
          toprv_id: {
            select: {
              docid: true,
              provnccode: true,
              description: true,
            },
          },
          tocry_id: {
            select: {
              docid: true,
              countrycode: true,
              description: true,
            },
          },
          tozcd_id: {
            select: {
              docid: true,
              zipcode: true,
              city: true,
              district: true,
              urban: true,
              subdistrict: true,
            },
          },
          tohem_id: {
            select: {
              docid: true,
              empcode: true,
              empname: true,
            },
          },
          sqm: true,
          tcurr_id: {
            select: {
              docid: true,
              curcode: true,
              description: true,
            },
          },
          topln_id: {
            select: {
              docid: true,
              pricecode: true,
              description: true,
              tcurr_id: {
                select: {
                  curcode: true,
                  description: true,
                },
              },
            },
          },
          storepic: true,
          tovat_id: {
            select: {
              docid: true,
              taxcode: true,
              rate: true,
            },
          },
          storeopen: true,
          statusactive: true,
          activated: true,
          tocsr: {
            select: {
              hwkey: true,
              token: true,
              email: true,
              statusactive: true,
              activated: true,
            },
          },
          prefixdoc: true,
          header01: true,
          header02: true,
          header03: true,
          header04: true,
          header05: true,
          footer01: true,
          footer02: true,
          footer03: true,
          footer04: true,
          footer05: true,
          sellingtax: true,
          openingbalance: true,
          // autorounding: true,
          // roundingvalue: true,
          // totalminus: true,
          // totalzero: true,
          // holdstruck: true,
          // holdclose: true,
          // autoprintstruk: true,
          // barcode1: true,
          // barcode2: true,
          // barcode3: true,
          // barcode4: true,
          // connectback: true,
          // maxuserkassa: true,
          // stocklevel: true,
          // minconst: true,
          // maxconst: true,
          // ordercycle: true,
          // taxby: true,
          // mtxline01: true,
          // mtxline02: true,
          // mtxline03: true,
          // mtxline04: true,
          // storepicpath: true,
          // autosync: true,
          // autodownload: true,
          // autodownload1: true,
          // autodownload2: true,
          // autodownload3: true,
          // autoupload: true,
          // vdfline1: true,
          // vdfline2: true,
          // vdfline1off: true,
          // vdfline2off: true,
          // searchitem: true,
          // roundingremarks: true,
          // checksellingprice: true,
          // qtyminusvalidation: true,
          // checkstockminus: true,
          // maxvoiddays: true,
          // attendacefp: true,
          // credittaxcode_id: {
          //   select: {
          //     docid: true,
          //     taxcode: true,
          //     rate: true,
          //   },
          // },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };

      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
