import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { torolData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET AUTHORIZATION BY ROLE
router.get(
  "/",
  pageQuery,
  query("torol_id").isUUID(),
  // authRoleMiddleware("f1026", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const { page, page_size, last_date, torol_id }: any = req.query;

      const rows = await prisma.taut1.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          updatedate: {
            gte: last_date,
          },
          torolId: await torolData(torol_id, userToken.gtentId),
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          tform_id: {
            select: {
              docid: true,
              formcode: true,
              description: true,
              descriptionfrgn: true,
            },
          },
          torol_id: {
            select: {
              docid: true,
              rolecode: true,
              rolename: true,
            },
          },
          authorization: true,
          canview: true,
          cancreate: true,
          canupdate: true,
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.taut1.count({
            where: {
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET AUTHORIZATION BY ROLE ALL
router.get(
  "/all",
  query("torol_id").isUUID(),
  // authRoleMiddleware("f1026", CAN_READ),
  async (req: Request, res: Response) => {
    const { torol_id }: any = req.query;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.taut1.findMany({
        where: {
          torolId: await torolData(torol_id, userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          tform_id: {
            select: {
              docid: true,
              formcode: true,
              description: true,
              descriptionfrgn: true,
            },
          },
          torol_id: {
            select: {
              docid: true,
              rolecode: true,
              rolename: true,
            },
          },
          authorization: true,
          canview: true,
          cancreate: true,
          canupdate: true,
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);
// * GET AUTHORIZATION BY ROLE - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-authorization-by-role?page=1&page_size=50&last_date=2021-01-01T00:00:00.000Z&torol_id=3b4d12c1-b3f6-4844-8e31-197a3587837c",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET AUTHORIZATION BY ROLE ALL - BOS
router.get("/bos/all", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-authorization-by-role/all?torol_id=3b4d12c1-b3f6-4844-8e31-197a3587837c",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

export default router;
