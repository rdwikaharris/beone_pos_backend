import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { toplnData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET PRICELIST PERIODS
router.get(
  "/",
  query("topln_id").isUUID(),
  pageQuery,
  query("search_string").optional().isString(),
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const { topln_id, page, page_size, last_date, search_string }: any =
        req.query;

      const rows = await prisma.tpln1.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          toplnId: await toplnData(topln_id, userToken.gtentId),
          tpln2: {
            some: {
              toitm_id: {
                OR: [
                  {
                    itemcode: {
                      contains: search_string ? search_string : undefined,
                    },
                  },
                  {
                    itemname: {
                      contains: search_string ? search_string : undefined,
                    },
                  },
                  {
                    tbitm: {
                      some: {
                        barcode: {
                          contains: search_string ? search_string : undefined,
                        },
                      },
                    },
                  },
                ],
              },
            },
          },
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          topln_id: {
            select: {
              docid: true,
            },
          },
          periodfr: true,
          periodto: true,
          baseprice: true,
          periodprice: true,
          factor: true,
          statusactive: true,
          activated: true,
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tpln1.count({
            where: {
              toplnId: await toplnData(topln_id, userToken.gtentId),
              tpln2: {
                some: {
                  toitm_id: {
                    OR: [
                      {
                        itemcode: {
                          contains: search_string ? search_string : undefined,
                        },
                      },
                      {
                        itemname: {
                          contains: search_string ? search_string : undefined,
                        },
                      },
                      {
                        tbitm: {
                          some: {
                            barcode: {
                              contains: search_string
                                ? search_string
                                : undefined,
                            },
                          },
                        },
                      },
                    ],
                  },
                },
              },
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET PRICELIST PERIODS - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-pricelist-period/?page=1&page_size=20&last_date=2023-10-05T06:00:07.000Z&topln_id=3d355ebf-ae71-4892-b09c-bdc8ae8f7331",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET PRICELIST PERIOD BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-pricelist-period/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET PRICELIST PERIOD BY IDS
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.tpln1.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          topln_id: {
            select: {
              docid: true,
            },
          },
          periodfr: true,
          periodto: true,
          baseprice: true,
          periodprice: true,
          factor: true,
          statusactive: true,
          activated: true,
          tpln2: {
            select: {
              docid: true,
              toitm_id: {
                select: {
                  docid: true,
                  itemcode: true,
                  itemname: true,
                },
              },
              tcurr_id: {
                select: {
                  docid: true,
                  curcode: true,
                  description: true,
                  descriptionfrgn: true,
                },
              },
              price: true,
              // purchaseprice: true,
              // calculatedprice: true,
              // marginvalue: true,
              // marginpercentage: true,
              // costprice: true,
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
