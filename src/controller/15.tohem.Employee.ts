import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { decodeToken } from "../utils/jwt";
import { catchError } from "../utils/console";
import { prisma } from "../config/prisma";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import { defaultTake, skip, totalPage } from "../config/variable";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET EMPLOYEES
router.get(
  "/",
  pageQuery,
  query("empname").optional().isString().isLength({ max: 200 }),
  query("search_string").optional().isString(),
  // authRoleMiddleware("f1021", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const { page, page_size, last_date, search_string }: any = req.query;

      let _rowQueryComposite = {};

      if (search_string !== undefined) {
        _rowQueryComposite = {
          OR: [
            {
              empcode: { contains: search_string },
            },
            {
              empname: { contains: search_string },
            },
          ],
        };
      }
      const rows = await prisma.tohem.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
          ..._rowQueryComposite,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          empcode: true,
          empname: true,
          email: true,
          phone: true,
          addr1: true,
          addr2: true,
          addr3: true,
          city: true,
          remarks: true,
          toprv_id: {
            select: {
              docid: true,
              provnccode: true,
              description: true,
            },
          },
          tocry_id: {
            select: {
              docid: true,
              countrycode: true,
              description: true,
            },
          },
          tozcd_id: {
            select: {
              docid: true,
              zipcode: true,
              city: true,
              district: true,
              urban: true,
              subdistrict: true,
            },
          },
          idcard: true,
          gender: true,
          birthdate: true,
          photo: true,
          joindate: true,
          resigndate: true,
          statusactive: true,
          activated: true,
          empdebt: true,
          emptitle: true,
          empworkplace: true,
          empdept: true,
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tohem.count({
            where: {
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
              ..._rowQueryComposite,
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET EMPLOYEES - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-employee?page=1&page_size=5&last_date=2021-01-01T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET EMPLOYEE BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-employee/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET EMPLOYEE BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1021", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.tohem.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          empcode: true,
          empname: true,
          email: true,
          phone: true,
          addr1: true,
          addr2: true,
          addr3: true,
          city: true,
          remarks: true,
          toprv_id: {
            select: {
              docid: true,
              provnccode: true,
              description: true,
            },
          },
          tocry_id: {
            select: {
              docid: true,
              countrycode: true,
              description: true,
            },
          },
          tozcd_id: {
            select: {
              docid: true,
              zipcode: true,
              city: true,
              district: true,
              urban: true,
              subdistrict: true,
            },
          },
          idcard: true,
          gender: true,
          birthdate: true,
          photo: true,
          joindate: true,
          resigndate: true,
          statusactive: true,
          activated: true,
          emptitle: true,
          empworkplace: true,
          empdept: true,
          empdebt: true,
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
