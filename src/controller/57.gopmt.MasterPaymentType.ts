import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip } from "../config/variable";
import { convertBigInt } from "../utils/jsonbigint";
import { param } from "express-validator";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET MASTER PAYMENT TYPE
router.get("/", pageQuery, async (req: Request, res: Response) => {
  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    const { page, page_size, last_date }: any = req.query;

    const rows = await prisma.gopmt.findMany({
      skip: skip(page, page_size),
      take: defaultTake(page_size),
      where: {
        updatedate: {
          gte: last_date,
        },
      },
      select: {
        docid: true,
        createdate: true,
        createby: true,
        updatedate: true,
        updateby: true,
        paytypecode: true,
        description: true,
      },
      orderBy: {
        id: "asc",
      },
    });

    resultObject = {
      statuscode: 200,
      data: convertBigInt(rows),
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET MASTER PAYMENT TYPE - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/global-payment-type/?page=1&page_size=20&last_date=2021-01-01T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET MASTER PAYMENT TYPE BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/global-payment-type/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET MASTER PAYMENT TYPE BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.gopmt.findUnique({
        where: { docid },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          paytypecode: true,
          description: true,
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
