import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { topmtData, tostrData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET MEANS OF PAYMENT
router.get(
  "/",
  pageQuery,
  query("store_id").isUUID(),
  query("payment_type").isUUID(),
  query("subtype").optional().isInt(),
  query("search_string").optional().isString(),
  // authRoleMiddleware("f1035", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const {
        store_id,
        payment_type,
        page,
        page_size,
        last_date,
        subtype,
        search_string,
      }: any = req.query;

      let _rowQueryComposite = {};

      if (search_string !== undefined) {
        _rowQueryComposite = {
          OR: [
            {
              mopcode: { contains: search_string },
            },
            {
              description: { contains: search_string },
            },
          ],
        };
      }

      const rows = await prisma.tpmt1.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          topmtId: await topmtData(payment_type, userToken.gtentId),
          updatedate: {
            gte: last_date,
          },
          subtype: Number(subtype),
          gtentId: await gtentData(userToken.gtentId),
          ..._rowQueryComposite,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          topmt_id: {
            select: {
              docid: true,
              paytypecode: true,
              description: true,
            },
          },
          subtype: true,
          tpmt3: {
            // where: {
            //   tostrId: await tostrData(store_id, userToken.gtentId),
            //   gtentId: await gtentData(userToken.gtentId),
            // },
            select: {
              docid: true,
              tpmt1_id: {
                select: {
                  docid: true,
                  mopcode: true,
                  description: true,
                  mopalias: true,
                  bankcharge: true,
                  consolidation: true,
                  credit: true,
                  subtype: true,
                },
              },
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tpmt1.count({
            where: {
              topmtId: await topmtData(payment_type, userToken.gtentId),
              updatedate: {
                gte: last_date,
              },
              subtype: Number(subtype),
              gtentId: await gtentData(userToken.gtentId),
              ..._rowQueryComposite,
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET MEANS OF PAYMENT - ALL
router.get(
  "/all",
  pageQuery,
  query("search_string").optional().isString(),
  // authRoleMiddleware("f1035", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const { page, page_size, last_date, search_string }: any = req.query;

      let _rowQueryComposite = {};

      if (search_string !== undefined) {
        _rowQueryComposite = {
          OR: [
            {
              mopcode: { contains: search_string },
            },
            {
              description: { contains: search_string },
            },
          ],
        };
      }

      const rows = await prisma.tpmt1.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
          ..._rowQueryComposite,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          topmt_id: {
            select: {
              docid: true,
              paytypecode: true,
              description: true,
            },
          },
          mopcode: true,
          description: true,
          mopalias: true,
          bankcharge: true,
          consolidation: true,
          credit: true,
          subtype: true,
          validforemp: true,
          tpmt3: {
            select: {
              docid: true,
              tpmt1_id: {
                select: {
                  docid: true,
                  mopcode: true,
                  description: true,
                  subtype: true,
                  validforemp: true,
                },
              },
              tostr_id: {
                select: {
                  docid: true,
                  storename: true,
                },
              },
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tpmt1.count({
            where: {
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
              ..._rowQueryComposite,
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET MEANS OF PAYMENT - PAYMENT
router.get(
  "/payment",
  query("payment_type").isUUID(),
  // authRoleMiddleware("f1035", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const { payment_type, page, page_size, last_date }: any = req.query;

      const rows = await prisma.tpmt1.findMany({
        // * skip: skip(page, page_size),
        // * take: defaultTake(page_size),
        where: {
          docid: payment_type,
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          topmt_id: {
            select: {
              docid: true,
              paytypecode: true,
              description: true,
            },
          },
          tpmt3: {
            select: {
              docid: true,
              tpmt1_id: {
                select: {
                  docid: true,
                  mopcode: true,
                  description: true,
                  mopalias: true,
                  bankcharge: true,
                  consolidation: true,
                  credit: true,
                },
              },
              tostr_id: {
                select: {
                  docid: true,
                  storecode: true,
                  storename: true,
                },
              },
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tpmt1.count({
            where: {
              docid: payment_type,
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET MEANS OF PAYMENT - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-means-of-payment/?page=1&page_size=2&last_date=2021-01-01T00:00:00.000Z&store_id=5a1a6cd8-a866-4d6a-b596-073ab86769c1&payment_type=f6cea872-8bdd-4240-a135-9b299f01a6df&subtype=0",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET MEANS OF PAYMENT - ALL - BOS
router.get("/bos/all", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-means-of-payment/all/?page=1&page_size=2&last_date=2021-01-01T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET MEANS OF PAYMENT - PAYMENT - BOS
router.get("/bos/payment", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-means-of-payment/payment/?page=1&page_size=2&last_date=2021-01-01T00:00:00.000Z&payment_type=63a21e05-9193-4b75-b391-90699d319aae",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET MEANS OF PAYMENT BY ID - BOS
router.get(
  "/bos/docid/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-means-of-payment/docid/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET MEANS OF PAYMENT BY ID
router.get(
  "/docid/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1035", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.tpmt1.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          topmt_id: {
            select: {
              docid: true,
              paytypecode: true,
              description: true,
            },
          },
          subtype: true,
          validforemp: true,
          tpmt3: {
            select: {
              docid: true,
              tpmt1_id: {
                select: {
                  docid: true,
                  mopcode: true,
                  description: true,
                  mopalias: true,
                  bankcharge: true,
                  consolidation: true,
                  credit: true,
                  subtype: true,
                  validforemp: true,
                },
              },
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
