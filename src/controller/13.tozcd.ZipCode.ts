import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { toprvData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import { defaultTake, skip, totalPage } from "../config/variable";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET ZIPCODES
router.get(
  "/",
  pageQuery,
  query("toprv_id").optional().isUUID(),
  query("search_string").optional().isString(),
  query(["zipcode", "city", "district", "subdistrict", "descriptionfrgn"])
    .optional()
    .isString(),
  // authRoleMiddleware("f1019", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const {
        toprv_id,
        page,
        page_size,
        last_date,
        search_string,
        zipcode,
        city,
        district,
        subdistrict,
        descriptionfrgn,
      }: any = req.query;

      let _rowQueryComposite = {};

      if (search_string !== undefined) {
        _rowQueryComposite = {
          OR: [
            {
              zipcode: { contains: search_string },
            },
            {
              city: { contains: search_string },
            },
            {
              district: { contains: search_string },
            },
            {
              urban: { contains: search_string },
            },
            {
              subdistrict: { contains: search_string },
            },
          ],
        };
      }

      const rows = await prisma.tozcd.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          toprvId:
            toprv_id !== undefined
              ? await toprvData(toprv_id, userToken.gtentId)
              : undefined,
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
          zipcode,
          city: {
            contains: city ? city : undefined,
          },
          district: {
            contains: district ? district : undefined,
          },
          subdistrict: {
            contains: subdistrict ? subdistrict : undefined,
          },
          toprv_id: {
            descriptionfrgn: {
              contains: descriptionfrgn ? descriptionfrgn : undefined,
            },
          },
          ..._rowQueryComposite,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          zipcode: true,
          city: true,
          district: true,
          urban: true,
          subdistrict: true,
          toprv_id: {
            select: {
              docid: true,
              provnccode: true,
              description: true,
              descriptionfrgn: true,
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tozcd.count({
            where: {
              toprvId:
                toprv_id !== undefined
                  ? await toprvData(toprv_id, userToken.gtentId)
                  : undefined,
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
              ..._rowQueryComposite,
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET ZIPCODES - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/global-master-zipcode?page=1&page_size=22&last_date=2021-01-01T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET ZIPCODE BY ID -BOS
router.get("/bos/:docid", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;
  const { docid } = req.params;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      `http://localhost:3001/global-master-zipcode/${docid}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET ZIPCODE BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1019", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const { docid } = req.params;
      const row = await prisma.tozcd.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          zipcode: true,
          city: true,
          district: true,
          urban: true,
          subdistrict: true,
          toprv_id: {
            select: {
              docid: true,
              provnccode: true,
              description: true,
              descriptionfrgn: true,
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
