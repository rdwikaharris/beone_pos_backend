import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import { param } from "express-validator";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET BILL OF MATERIAL
router.get(
  "/",
  pageQuery,
  // authRoleMiddleware("f1041", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const { page, page_size, last_date }: any = req.query;

      const rows = await prisma.toitt.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          toitm_id: {
            select: {
              docid: true,
              itemcode: true,
              itemname: true,
            },
          },
          quantity: true,
          touom_id: {
            select: {
              docid: true,
              uomcode: true,
              uomdesc: true,
            },
          },
          tipe: true,
          // tcurr_id: {
          //   select: {
          //     docid: true,
          //     curcode: true,
          //     description: true,
          //   },
          // },
          // price: true,
          // statusactive: true,
          titt1: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              toitm_id: {
                select: {
                  docid: true,
                  itemcode: true,
                  itemname: true,
                },
              },
              quantity: true,
              touom_id: {
                select: {
                  docid: true,
                  uomcode: true,
                  uomdesc: true,
                },
              },
              // tcurr_id: {
              //   select: {
              //     docid: true,
              //     curcode: true,
              //     description: true,
              //   },
              // },
              // price: true,
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.toitt.count({
            where: {
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);
// * GET BILL OF MATERIAL - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-bill-of-material?page=1&page_size=2&last_date=2021-10-06T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET BILL OF MATERIAL BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-bill-of-material/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET BILL OF MATERIAL BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1041", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.toitt.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          // tcurr_id: {
          //   select: {
          //     docid: true,
          //     curcode: true,
          //     description: true,
          //   },
          // },
          // price: true,
          // statusactive: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          toitm_id: {
            select: {
              docid: true,
              itemcode: true,
              itemname: true,
            },
          },
          quantity: true,
          touom_id: {
            select: {
              docid: true,
              uomcode: true,
              uomdesc: true,
            },
          },
          tipe: true,
          titt1: {
            select: {
              docid: true,
              createdate: true,
              createby: true,
              updatedate: true,
              updateby: true,
              toitm_id: {
                select: {
                  docid: true,
                  itemcode: true,
                  itemname: true,
                },
              },
              quantity: true,
              touom_id: {
                select: {
                  docid: true,
                  uomcode: true,
                  uomdesc: true,
                },
              },
              // tcurr_id: {
              //   select: {
              //     docid: true,
              //     curcode: true,
              //     description: true,
              //   },
              // },
              // price: true,
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
