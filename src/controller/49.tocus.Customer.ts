import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET CUSTOMER
router.get(
  "/",
  pageQuery,
  query("custname").optional().isString().isLength({ max: 100 }),
  query("search_string").optional().isString(),
  // authRoleMiddleware("f1036", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = decodeToken(
        String(req.headers.authorization)
      );
      const { page, page_size, last_date, custname, search_string }: any =
        req.query;
      let _rowQueryComposite = {};

      if (search_string !== undefined) {
        _rowQueryComposite = {
          OR: [
            {
              custcode: { contains: search_string },
            },
            {
              custname: { contains: search_string },
            },
          ],
        };
      }

      const rows = await prisma.tocus.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          custname: {
            contains: custname,
          },
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
          ..._rowQueryComposite,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          custcode: true,
          custname: true,
          tocrg_id: {
            select: {
              docid: true,
              custgroupcode: true,
              description: true,
            },
          },
          idcard: true,
          taxno: true,
          gender: true,
          birthdate: true,
          addr1: true,
          addr2: true,
          addr3: true,
          city: true,
          toprv_id: {
            select: {
              docid: true,
              provnccode: true,
              description: true,
              descriptionfrgn: true,
            },
          },
          tocry_id: {
            select: {
              docid: true,
              countrycode: true,
              description: true,
              descriptionfrgn: true,
              phonecode: true,
            },
          },
          tozcd_id: {
            select: {
              docid: true,
              zipcode: true,
              city: true,
              district: true,
              urban: true,
              subdistrict: true,
            },
          },
          phone: true,
          email: true,
          remarks: true,
          toptr_id: {
            select: {
              docid: true,
              paymentcode: true,
              description: true,
            },
          },
          topln_id: {
            select: {
              docid: true,
              pricecode: true,
              description: true,
            },
          },
          maxdiscount: true,
          statusactive: true,
          activated: true,
          isemployee: true,
          tohem_id: {
            select: {
              docid: true,
              empcode: true,
              empname: true,
            },
          },
          // docid_crm: true,
          tcus1: {
            select: {
              docid: true,
              linenum: true,
              addr1: true,
              addr2: true,
              addr3: true,
              city: true,
              toprv_id: {
                select: {
                  docid: true,
                  provnccode: true,
                  description: true,
                  descriptionfrgn: true,
                },
              },
              tocry_id: {
                select: {
                  docid: true,
                  countrycode: true,
                  description: true,
                  descriptionfrgn: true,
                },
              },
              tozcd_id: {
                select: {
                  docid: true,
                  zipcode: true,
                  city: true,
                  district: true,
                  urban: true,
                  subdistrict: true,
                },
              },
            },
          },
          tcus2: {
            select: {
              docid: true,
              linenum: true,
              title: true,
              fullname: true,
              phone: true,
              email: true,
              position: true,
              idcard: true,
              taxno: true,
              gender: true,
              birthdate: true,
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tocus.count({
            where: {
              custname: {
                contains: custname,
              },
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
              ..._rowQueryComposite,
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET CUSTOMER - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-customer/?page=1&page_size=50&last_date=2023-06-01T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET CUSTOMER BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-customer/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET CUSTOMER BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1036", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.tocus.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          custcode: true,
          custname: true,
          tocrg_id: {
            select: {
              docid: true,
              custgroupcode: true,
              description: true,
            },
          },
          idcard: true,
          taxno: true,
          gender: true,
          birthdate: true,
          addr1: true,
          addr2: true,
          addr3: true,
          city: true,
          toprv_id: {
            select: {
              docid: true,
              provnccode: true,
              description: true,
              descriptionfrgn: true,
            },
          },
          tocry_id: {
            select: {
              docid: true,
              countrycode: true,
              description: true,
              descriptionfrgn: true,
              phonecode: true,
            },
          },
          tozcd_id: {
            select: {
              docid: true,
              zipcode: true,
              city: true,
              district: true,
              urban: true,
              subdistrict: true,
            },
          },
          phone: true,
          email: true,
          remarks: true,
          toptr_id: {
            select: {
              docid: true,
              paymentcode: true,
              description: true,
            },
          },
          topln_id: {
            select: {
              docid: true,
              pricecode: true,
              description: true,
            },
          },
          maxdiscount: true,
          statusactive: true,
          activated: true,
          isemployee: true,
          tohem_id: {
            select: {
              docid: true,
              empcode: true,
              empname: true,
            },
          },
          // docid_crm: true,
          tcus1: {
            select: {
              docid: true,
              linenum: true,
              addr1: true,
              addr2: true,
              addr3: true,
              city: true,
              toprv_id: {
                select: {
                  docid: true,
                  provnccode: true,
                  description: true,
                  descriptionfrgn: true,
                },
              },
              tocry_id: {
                select: {
                  docid: true,
                  countrycode: true,
                  description: true,
                  descriptionfrgn: true,
                },
              },
              tozcd_id: {
                select: {
                  docid: true,
                  zipcode: true,
                  city: true,
                  district: true,
                  urban: true,
                  subdistrict: true,
                },
              },
            },
          },
          tcus2: {
            select: {
              docid: true,
              linenum: true,
              title: true,
              fullname: true,
              phone: true,
              email: true,
              position: true,
              idcard: true,
              taxno: true,
              gender: true,
              birthdate: true,
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
