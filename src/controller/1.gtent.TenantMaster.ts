import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { prisma } from "../config/prisma";
import { convertBigInt } from "../utils/jsonbigint";
import axios from "axios";
import fetchBosToken from "../utils/fetchToken";
import { decodeToken } from "../utils/jwt";
import { defaultTake, skip, totalPage } from "../config/variable";
import { param } from "express-validator";
import { catchError } from "../utils/console";

const router = Router();
let resultObject: ResultObject;

// * GET TENANT
router.get("/", pageQuery, async (req: Request, res: Response) => {
  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );

    const { page, page_size, last_date }: any = req.query;
    const rows = await prisma.gtent.findMany({
      skip: skip(page, page_size),
      take: defaultTake(page_size),
      where: {
        updatedate: {
          gte: last_date,
        },
      },
      select: {
        docid: true,
        createdate: true,
        createby: true,
        updatedate: true,
        updateby: true,
        compcode: true,
        compname: true,
        owner: true,
        brand: true,
        othindustry: true,
        email: true,
        phone: true,
        addr1: true,
        addr2: true,
        city: true,
        // glind_id: {
        //   select: {
        //     docid: true,
        //   },
        // },
        // gcmsz_id: {
        //   select: {
        //     docid: true,
        //     description: true,
        //     descriptionfrgn: true,
        //   },
        // },
        // gprvn_id: {
        //   select: {
        //     docid: true,
        //     provcode: true,
        //     description: true,
        //     descriptionfrgn: true,
        //   },
        // },
        // gcntr_id: {
        //   select: {
        //     docid: true,
        //     countrycode: true,
        //     description: true,
        //     descriptionfrgn: true,
        //   },
        // },
        // gzipc_id: {
        //   select: {
        //     docid: true,
        //     zipcode: true,
        //     city: true,
        //     district: true,
        //     subdistrict: true,
        //     urban: true,
        //   },
        // },
        itemcodelength: true,
        batchlength: true,
        synccrm: true,
      },
      orderBy: {
        id: "asc",
      },
    });

    if (page_size) {
      const _counter = await prisma.gtent.count({
        where: {
          updatedate: {
            gte: last_date,
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: {
          rows: convertBigInt(rows),
          total_page: totalPage(_counter, defaultTake(page_size)),
        },
      };
    } else {
      resultObject = {
        statuscode: 200,
        data: convertBigInt(rows),
      };
    }

    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET TENANT - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/global-tenant-master?page=1&page_size=1&last_date=2021-01-01T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET TENANT BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/global-tenant-master/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET TENANT BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const data = await prisma.gtent.findFirst({
        where: {
          docid,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          compcode: true,
          compname: true,
          owner: true,
          brand: true,
          othindustry: true,
          email: true,
          phone: true,
          addr1: true,
          addr2: true,
          city: true,
          // glind_id: {
          //   select: {
          //     docid: true,
          //   },
          // },
          // gcmsz_id: {
          //   select: {
          //     docid: true,
          //     description: true,
          //     descriptionfrgn: true,
          //   },
          // },
          // gprvn_id: {
          //   select: {
          //     docid: true,
          //     provcode: true,
          //     description: true,
          //     descriptionfrgn: true,
          //   },
          // },
          // gcntr_id: {
          //   select: {
          //     docid: true,
          //     countrycode: true,
          //     description: true,
          //     descriptionfrgn: true,
          //   },
          // },
          // gzipc_id: {
          //   select: {
          //     docid: true,
          //     zipcode: true,
          //     city: true,
          //     district: true,
          //     subdistrict: true,
          //     urban: true,
          //   },
          // },
          itemcodelength: true,
          batchlength: true,
          synccrm: true,
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(data),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
