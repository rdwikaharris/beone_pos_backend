import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { toitmData, tostrData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import axios from "axios";
import fetchBosToken from "../utils/fetchToken";

const router = Router();
let resultObject: ResultObject;

// * GET ITEM BY STORES
router.get(
  "/",
  pageQuery,
  query("store_id").optional().isUUID(),
  query("toitm_id").optional().isUUID(),
  query("popitem").optional().isIn([0, 1]),
  // authRoleMiddleware("f1029", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const { store_id, toitm_id, page, page_size, last_date, popitem }: any =
        req.query;

      const rows = await prisma.tsitm.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          tostrId:
            store_id !== undefined
              ? await tostrData(store_id, userToken.gtentId)
              : undefined,
          toitmId:
            toitm_id !== undefined
              ? await toitmData(toitm_id, userToken.gtentId)
              : undefined,
          // toitm_id: {
          //   popitem: popitem ? Number(popitem) : undefined,
          // },
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          toitm_id: {
            select: {
              docid: true,
              itemcode: true,
              itemname: true,
              invitem: true,
              serialno: true,
              tocat_id: {
                select: {
                  docid: true,
                  catcode: true,
                  catname: true,
                },
              },
              touom_id: {
                select: {
                  docid: true,
                  uomcode: true,
                  uomdesc: true,
                },
              },
              minstock: true,
              maxstock: true,
              includetax: true,
              remarks: true,
              statusactive: true,
              activated: true,
              isbatch: true,
              openprice: true,
              // popitem: true,
              // bpom: true,
              // expdate: true,
              // multiplyorder: true,
              // margin: true,
              // memberdiscount: true,
              // mergequantity: true,
              property_1: {
                select: {
                  docid: true,
                  properties: true,
                  code: true,
                  description: true,
                },
              },
              property_2: {
                select: {
                  docid: true,
                  properties: true,
                  code: true,
                  description: true,
                },
              },
              property_3: {
                select: {
                  docid: true,
                  properties: true,
                  code: true,
                  description: true,
                },
              },
              property_4: {
                select: {
                  docid: true,
                  properties: true,
                  code: true,
                  description: true,
                },
              },
              property_5: {
                select: {
                  docid: true,
                  properties: true,
                  code: true,
                  description: true,
                },
              },
              property_6: {
                select: {
                  docid: true,
                  properties: true,
                  code: true,
                  description: true,
                },
              },
              property_7: {
                select: {
                  docid: true,
                  properties: true,
                  code: true,
                  description: true,
                },
              },
              property_8: {
                select: {
                  docid: true,
                  properties: true,
                  code: true,
                  description: true,
                },
              },
              property_9: {
                select: {
                  docid: true,
                  properties: true,
                  code: true,
                  description: true,
                },
              },
              property_10: {
                select: {
                  docid: true,
                  properties: true,
                  code: true,
                  description: true,
                },
              },
              tbitm: {
                select: {
                  docid: true,
                  barcode: true,
                  quantity: true,
                  touom_id: {
                    select: {
                      docid: true,
                      uomcode: true,
                    },
                  },
                  statusactive: true,
                },
              },
            },
          },
          tostr_id: {
            select: {
              docid: true,
              storecode: true,
              storename: true,
            },
          },
          statusactive: true,
          activated: true,
          tovat_id: {
            select: {
              docid: true,
              taxcode: true,
              description: true,
            },
          },
          tovat_id_pur: {
            select: {
              docid: true,
              taxcode: true,
              description: true,
            },
          },
          // minstock: true,
          // maxstock: true,
          // multiplyorder: true,
          // price: true,
          // marginprice: true,
          // marginpercentage: true,
          tvitm: {
            select: {
              // toven_id: {
              //   select: {
              //     docid: true,
              //     vendcode: true,
              //     vendname: true,
              //   },
              // },
              listing: true,
              minorder: true,
              multipyorder: true,
              canorder: true,
              dflt: true,
            },
          },
          // tsbin: {
          //   select: {
          //     docid: true,
          //     tbin1_id: {
          //       select: {
          //         docid: true,
          //         bincode: true,
          //         description: true,
          //       },
          //     },
          //     dflt: true,
          //     statusactive: true,
          //   },
          // },
        },
        orderBy: {
          id: "asc",
        },
      });

      let newRows: any[] = [];

      // for (const row in rows) {
      //   const {
      //     toitm_id,
      //     tostr_id,
      //     price,
      //     marginprice,
      //     marginpercentage,
      //     ...body
      //   } = rows[row];

      //   const itempriceData = await axios
      //     .get(
      //       `${process.env.INTERFACING_URL}/proxy/last-item-purchase-price/`,
      //       {
      //         params: {
      //           itemcode: toitm_id?.itemcode,
      //         },
      //       }
      //     )
      //     .then((d) => {
      //       return parseFloat(d.data.result[0].Price);
      //     })
      //     .catch((d) => {
      //       return 0;
      //     });

      //   const itemcostData = await axios
      //     .get(`${process.env.INTERFACING_URL}/proxy/item-cost/`, {
      //       params: {
      //         itemcode: toitm_id?.itemcode,
      //       },
      //     })
      //     .then((d) => {
      //       return d.data.result[0];
      //     })
      //     .catch((d) => {
      //       return 0;
      //     });

      //   const storeData = await prisma.tostr.findFirst({
      //     where: {
      //       docid: tostr_id?.docid,
      //     },
      //   });

      //   const itemPriceList = await prisma.tpln2.findFirst({
      //     where: {
      //       toitm_id: {
      //         docid: toitm_id?.docid,
      //       },
      //       tpln1_id: {
      //         toplnId: storeData?.toplnId,
      //       },
      //     },
      //     select: {
      //       price: true,
      //       marginvalue: true,
      //       marginpercentage: true,
      //     },
      //     orderBy: {
      //       updatedate: "desc",
      //     },
      //   });

      //   if (
      //     toitm_id?.tocat_id?.catcode !== null ||
      //     toitm_id?.tocat_id?.catcode !== undefined
      //   ) {
      //     const _itemHierarchy: object = {
      //       ItemHierarchy: await ItemHierarchy(
      //         toitm_id?.tocat_id?.catcode,
      //         await gtentData(userToken.gtentId)
      //       ),
      //     };

      //     newRows[row] = {
      //       toitm_id: {
      //         ...toitm_id,
      //         ..._itemHierarchy,
      //         purchaseprice: itempriceData ? itempriceData : 0,
      //         itemcost: itemcostData ? itemcostData : 0,
      //       },
      //       tostr_id,
      //       price: itemPriceList?.price ? itemPriceList.price : 0,
      //       marginprice: itemPriceList?.marginvalue
      //         ? parseFloat(itemPriceList.marginvalue.toString())
      //         : 0,
      //       marginpercentage: itemPriceList?.marginpercentage
      //         ? parseFloat(itemPriceList.marginpercentage.toString())
      //         : 0,
      //       ...body,
      //     };
      //   } else {
      //     newRows[row] = {
      //       toitm_id: {
      //         ...toitm_id,
      //         purchaseprice: itempriceData ? itempriceData : 0,
      //         itemcost: itemcostData ? itemcostData : 0,
      //       },
      //       tostr_id,
      //       price: itemPriceList?.price ? itemPriceList.price : 0,
      //       marginprice: itemPriceList?.marginvalue
      //         ? parseFloat(itemPriceList.marginvalue.toString())
      //         : 0,
      //       marginpercentage: itemPriceList?.marginpercentage
      //         ? parseFloat(itemPriceList.marginpercentage.toString())
      //         : 0,
      //       ...body,
      //     };
      //   }
      // }

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tsitm.count({
            where: {
              tostrId:
                store_id !== undefined
                  ? await tostrData(store_id, userToken.gtentId)
                  : undefined,
              toitmId:
                toitm_id !== undefined
                  ? await toitmData(toitm_id, userToken.gtentId)
                  : undefined,
              // toitm_id: {
              //   popitem: popitem ? Number(popitem) : undefined,
              // },
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(newRows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(newRows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET ITEM BY STORES - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;
  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-item-by-store?store_id=5a1a6cd8-a866-4d6a-b596-073ab86769c1&page=1&page_size=1&fillvendor=true",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET ITEM BY STORE BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-item-by-store/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET ITEM BY STORE BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1029", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.tsitm.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          toitm_id: {
            select: {
              docid: true,
              itemcode: true,
              itemname: true,
              invitem: true,
              serialno: true,
              tocat_id: {
                select: {
                  docid: true,
                  catcode: true,
                  catname: true,
                },
              },
              touom_id: {
                select: {
                  docid: true,
                  uomcode: true,
                  uomdesc: true,
                },
              },
              minstock: true,
              maxstock: true,
              includetax: true,
              remarks: true,
              statusactive: true,
              activated: true,
              isbatch: true,
              openprice: true,
              tbitm: {
                select: {
                  docid: true,
                  barcode: true,
                  quantity: true,
                  touom_id: {
                    select: {
                      docid: true,
                      uomcode: true,
                    },
                  },
                  statusactive: true,
                },
              },
            },
          },
          tostr_id: {
            select: {
              docid: true,
              storecode: true,
              storename: true,
            },
          },
          statusactive: true,
          activated: true,
          tovat_id: {
            select: {
              docid: true,
              taxcode: true,
              description: true,
            },
          },
          tovat_id_pur: {
            select: {
              docid: true,
              taxcode: true,
              description: true,
            },
          },
          // minstock: true,
          // maxstock: true,
          // multiplyorder: true,
          // price: true,
          // marginprice: true,
          // marginpercentage: true,
          tvitm: {
            select: {
              // toven_id: {
              //   select: {
              //     docid: true,
              //     vendcode: true,
              //     vendname: true,
              //   },
              // },
              listing: true,
              minorder: true,
              multipyorder: true,
              canorder: true,
              dflt: true,
            },
          },
          // tsbin: {
          //   select: {
          //     docid: true,
          //     tbin1_id: {
          //       select: {
          //         docid: true,
          //         bincode: true,
          //         description: true,
          //       },
          //     },
          //     dflt: true,
          //     statusactive: true,
          //   },
          // },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
