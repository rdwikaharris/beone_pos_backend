import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { tocsrData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET CASH BALANCE TRANSACTIONS
router.get(
  "/",
  pageQuery,
  query("tocsr_id").optional().isUUID(),
  query("search_docnum").optional().isString(),
  // authRoleMiddleware("f1103", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const { tocsr_id, page, page_size, last_date, search_docnum }: any =
        req.query;

      const rows = await prisma.tcsr1.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          tocsrId:
            tocsr_id !== undefined
              ? await tocsrData(tocsr_id, userToken.gtentId)
              : undefined,
          updatedate: {
            gte: last_date,
          },
          docnum: {
            contains: search_docnum,
          },
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          tocsr_id: {
            select: {
              docid: true,
              tostr_id: {
                select: {
                  docid: true,
                  storecode: true,
                  storename: true,
                },
              },
              description: true,
            },
          },
          tousr_id: {
            select: {
              docid: true,
              username: true,
              trolleyuser: true,
            },
          },
          docnum: true,
          opendate: true,
          opentime: true,
          calcdate: true,
          calctime: true,
          closedate: true,
          closetime: true,
          timezone: true,
          openvalue: true,
          calcvalue: true,
          cashvalue: true,
          closevalue: true,
          openedby_id: {
            select: {
              docid: true,
              username: true,
              trolleyuser: true,
            },
          },
          closedby_id: {
            select: {
              docid: true,
              username: true,
              trolleyuser: true,
            },
          },
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tcsr1.count({
            where: {
              tocsrId:
                tocsr_id !== undefined
                  ? await tocsrData(tocsr_id, userToken.gtentId)
                  : undefined,
              updatedate: {
                gte: last_date,
              },
              docnum: {
                contains: search_docnum,
              },
              gtentId: await gtentData(userToken.gtentId),
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET CASH BALANCE TRANSACTIONS - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-cashier-balance-transaction?page=1&page_size=20&last_date=2021-10-06T00:00:00.000Z",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET CASH BALANCE TRANSACTION BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-cashier-balance-transaction/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET CASH BALANCE TRANSACTION BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1103", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.tcsr1.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          tocsr_id: {
            select: {
              docid: true,
              tostr_id: {
                select: {
                  docid: true,
                  storecode: true,
                  storename: true,
                },
              },
              description: true,
            },
          },
          tousr_id: {
            select: {
              docid: true,
              username: true,
              trolleyuser: true,
            },
          },
          docnum: true,
          opendate: true,
          opentime: true,
          calcdate: true,
          calctime: true,
          closedate: true,
          closetime: true,
          timezone: true,
          openvalue: true,
          calcvalue: true,
          cashvalue: true,
          closevalue: true,
          openedby_id: {
            select: {
              docid: true,
              username: true,
              trolleyuser: true,
            },
          },
          closedby_id: {
            select: {
              docid: true,
              username: true,
              trolleyuser: true,
            },
          },
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
