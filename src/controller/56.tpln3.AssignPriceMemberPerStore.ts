import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { toplnData, tostrData } from "../utils/docid-id/tenant_lookup_1";
import { gtentData } from "../utils/docid-id/global_lookup";
import { convertBigInt } from "../utils/jsonbigint";
import { pageQuery } from "../middleware";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET APMPS
router.get(
  "/",
  query("topln_id").optional().isUUID(),
  pageQuery,
  query("tostr_id").optional().isUUID(),
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = decodeToken(
        String(req.headers.authorization)
      );
      const { topln_id, page, page_size, last_date, tostr_id }: any = req.query;

      const rows = await prisma.tpln3.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          toplnId:
            topln_id !== undefined
              ? await toplnData(topln_id, userToken.gtentId)
              : undefined,
          tostrId: tostr_id
            ? await tostrData(tostr_id, userToken.gtentId)
            : undefined,
          updatedate: {
            gte: last_date,
          },
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          topln_id: {
            select: {
              docid: true,
              description: true,
            },
          },
          tostr_id: {
            select: {
              docid: true,
              storecode: true,
              storename: true,
            },
          },
          statusactive: true,
          activated: true,
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tpln3.count({
            where: {
              toplnId:
                topln_id !== undefined
                  ? await toplnData(topln_id, userToken.gtentId)
                  : undefined,
              tostrId: tostr_id
                ? await tostrData(tostr_id, userToken.gtentId)
                : undefined,
              updatedate: {
                gte: last_date,
              },
              gtentId: await gtentData(userToken.gtentId),
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET APMPS - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-assign-price-member-per-store/?topln_id=3d355ebf-ae71-4892-b09c-bdc8ae8f7331&page=1&page_size=5&last_date=2021-10-06T00:00:00.000Z&tostr_id=878694e6-fdf4-49a7-82e3-d0facb685741",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET APMPS BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-assign-price-member-per-store/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET APMPS BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = decodeToken(
        String(req.headers.authorization)
      );

      const row = await prisma.tpln3.findFirst({
        where: {
          docid,
          gtentId: await gtentData(userToken.gtentId),
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          topln_id: {
            select: {
              docid: true,
            },
          },
          tostr_id: {
            select: {
              docid: true,
            },
          },
          statusactive: true,
          activated: true,
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
