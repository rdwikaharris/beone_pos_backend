import { Request, Response, Router } from "express";
import { ResultObject, UserToken } from "../interface";
import { pageQuery } from "../middleware";
import { param, query } from "express-validator";
import { catchError } from "../utils/console";
import { decodeToken } from "../utils/jwt";
import { userState } from "../functions/helper";
import { prisma } from "../config/prisma";
import { defaultTake, skip, totalPage } from "../config/variable";
import { tpln2Data } from "../utils/docid-id/tenant_lookup_1";
import { convertBigInt } from "../utils/jsonbigint";
import fetchBosToken from "../utils/fetchToken";
import axios from "axios";

const router = Router();
let resultObject: ResultObject;

// * GET PRICE BY ITEM BARCODE
router.get(
  "/",
  query("tpln2_id").isUUID(),
  pageQuery,
  query("search_item").optional().isString(),
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      const currentState = await userState(userToken);
      const { tpln2_id, page, page_size, last_date }: any = req.query;

      const rows = await prisma.tpln4.findMany({
        skip: skip(page, page_size),
        take: defaultTake(page_size),
        where: {
          tpln2Id: await tpln2Data(tpln2_id, userToken.gtentId),
          updatedate: {
            gte: last_date,
          },
          gtentId: currentState.tenant,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          tpln2_id: {
            select: {
              docid: true,
              toitm_id: {
                select: {
                  docid: true,
                  itemcode: true,
                  itemname: true,
                  touom_id: {
                    select: {
                      docid: true,
                      uomcode: true,
                      uomdesc: true,
                    },
                  },
                },
              },
            },
          },
          tbitm_id: {
            select: {
              docid: true,
              barcode: true,
              statusactive: true,
              quantity: true,
              touom_id: {
                select: {
                  docid: true,
                  uomcode: true,
                  uomdesc: true,
                },
              },
            },
          },
          // tcurr_id: {
          //   select: {
          //     docid: true,
          //     curcode: true,
          //     description: true,
          //   },
          // },
          price: true,
        },
        orderBy: {
          id: "asc",
        },
      });

      if (page_size) {
        const [_counter] = await prisma.$transaction([
          prisma.tpln4.count({
            where: {
              tpln2Id: await tpln2Data(tpln2_id, userToken.gtentId),
              updatedate: {
                gte: last_date,
              },
              gtentId: currentState.tenant,
            },
          }),
        ]);

        resultObject = {
          statuscode: 200,
          data: {
            rows: convertBigInt(rows),
            current_page: Number(page),
            total_page: totalPage(_counter, defaultTake(page_size)),
            total: _counter,
          },
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      } else {
        resultObject = {
          statuscode: 200,
          data: convertBigInt(rows),
        };
        return res.status(resultObject.statuscode).json(resultObject.data);
      }
    } catch (err) {
      catchError;
    }
  }
);

// * GET PRICE BY ITEM BARCODE - BOS
router.get("/bos", async (req: Request, res: Response) => {
  let token = null;
  const { password } = req.body;

  try {
    const userToken: UserToken = await decodeToken(
      String(req.headers.authorization)
    );
    if (!token) token = await fetchBosToken(userToken.email, password);

    const { data } = await axios.get(
      "http://localhost:3001/tenant-price-by-item-barcode/?page=1&page_size=5&tpln2_id=2bf74706-780f-448f-a351-1b87cc69069d",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    resultObject = {
      statuscode: 200,
      data,
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } catch (err) {
    catchError(err, res);
  }
});

// * GET PRICE BY ITEM BARCODE BY ID - BOS
router.get(
  "/bos/:docid",
  param("docid").isUUID(),
  async (req: Request, res: Response) => {
    let token = null;
    const { password } = req.body;
    const { docid } = req.params;

    try {
      const userToken: UserToken = await decodeToken(
        String(req.headers.authorization)
      );
      if (!token) token = await fetchBosToken(userToken.email, password);

      const { data } = await axios.get(
        `http://localhost:3001/tenant-price-by-item-barcode/${docid}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      resultObject = {
        statuscode: 200,
        data,
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

// * GET PRICE BY ITEM BARCODE BY ID
router.get(
  "/:docid",
  param("docid").isUUID(),
  // authRoleMiddleware("f1031", CAN_READ),
  async (req: Request, res: Response) => {
    const { docid } = req.params;
    try {
      const userToken: UserToken = decodeToken(
        String(req.headers.authorization)
      );
      const currentState = await userState(userToken);

      const row = await prisma.tpln4.findFirst({
        where: {
          docid,
          gtentId: currentState.tenant,
        },
        select: {
          docid: true,
          createdate: true,
          createby: true,
          updatedate: true,
          updateby: true,
          gtent_id: {
            select: {
              docid: true,
            },
          },
          tpln2_id: {
            select: {
              docid: true,
              toitm_id: {
                select: {
                  docid: true,
                  itemcode: true,
                  itemname: true,
                  touom_id: {
                    select: {
                      docid: true,
                      uomcode: true,
                      uomdesc: true,
                    },
                  },
                },
              },
            },
          },
          tbitm_id: {
            select: {
              docid: true,
              barcode: true,
              statusactive: true,
              quantity: true,
              touom_id: {
                select: {
                  docid: true,
                  uomcode: true,
                  uomdesc: true,
                },
              },
            },
          },
          // tcurr_id: {
          //   select: {
          //     docid: true,
          //     curcode: true,
          //     description: true,
          //   },
          // },
          price: true,
        },
        orderBy: {
          id: "asc",
        },
      });

      resultObject = {
        statuscode: 200,
        data: convertBigInt(row),
      };
      return res.status(resultObject.statuscode).json(resultObject.data);
    } catch (err) {
      catchError(err, res);
    }
  }
);

export default router;
