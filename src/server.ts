import express from "express";
import * as dotenv from "dotenv";
import root from "./router";
import {
  // JWTCheck,
  morganMiddleware,
  morganDevMiddleware,
  unauthorizedErrorMiddleware,
} from "./middleware";
import { ENV } from "./constant/env";
import cors, { CorsOptions } from "cors";

async function main() {
  dotenv.config();

  // const corsConfig: CorsOptions = {
  //   origin: "*",
  //   credentials: true,
  //   allowedHeaders: ["Content-Type", "Authorization"],
  //   methods: ["GET", "HEAD", "PUT", "PATCH", "POST", "DELETE"],
  // };

  const app = express();

  app.use(express.json({ limit: "3mb" }));
  app.use(express.urlencoded({ extended: true, limit: "3mb" }));
  app.use(morganDevMiddleware);
  // app.use(JWTCheck);
  app.use(morganMiddleware);
  // app.use(morganDevMiddleware);
  app.use(unauthorizedErrorMiddleware);
  // app.use(cors(corsConfig));
  app.use(root);

  app
    .listen(process.env.PORT, () => {
      console.log("Server Up and Running at Port:" + Number(ENV.PORT));
    })
    .setTimeout(20 * 60 * 1000);
}

main().catch((e) => {
  console.error(e);
});
