import { NextFunction, Request, RequestHandler, Response } from "express";
import { query, validationResult } from "express-validator";
import { ResultObject } from "../interface";
import * as dotenv from "dotenv";
import jwt from "express-jwt";
import morgan from "morgan";
import * as rfs from "rotating-file-stream";

import { prisma } from "../config/prisma";
import { UserToken } from "../interface";
import { accessCodeConverter } from "../utils/converter";
import { decodeToken } from "../utils/jwt";
import { ENV } from "../constant/env";

let resultObject: ResultObject;

dotenv.config();

export const pageQuery: RequestHandler[] = [
  query("page").isInt(),
  query("page_size").optional().isInt(),
  query("last_date").optional().isISO8601(),
];

export const validatePage = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const err = validationResult(req);

  if (!err.isEmpty()) {
    resultObject = {
      statuscode: 400,
      data: err.array(),
    };
    // console.log(err);
    return res.status(resultObject.statuscode).json(resultObject.data);
  }
  next();
};

export const usersQuery: RequestHandler[] = [
  query("page").isInt(),
  query("page_size").optional().isInt(),
  query("last_date").optional().isISO8601(),
  query("username").optional().isString(),
  query("email").optional().isString(),
  query("empcode").optional().isString(),
  query("empname").optional().isString(),
  query("rolecode").optional().isString(),
  query(["username", "email", "empcode", "empname", "rolecode"])
    .optional()
    .isString(),
  query(["superuser", "statusactive"]).optional().isIn([1, 0]),
];

// // * JWT Check
// export const JWTCheck = jwt({
//   secret: String(ENV.SECRET_KEY),
//   algorithms: ["HS256"],
// }).unless({
//   path: [
//     "/auth/login",
//     /template\/*/,
//     /xlsx\/template*/,
//     /forgot-password\/*/,
//     /crm\/tenant-item-master\/*/,
//   ],
// });

// * Access Log Stream
const accessLogStream = rfs.createStream("activity.log", {
  interval: "1d",
  path: "log",
});

// * Morgan Middleware
export const morganMiddleware = morgan("combined", {
  skip: function (req: Request, res: Response) {
    return res.statusCode <= 400;
  },
  stream: accessLogStream,
});

export const morganDevMiddleware = morgan("dev", {});

// * Unauthorized Middleware
export function unauthorizedErrorMiddleware(
  err: any,
  req: any,
  res: any,
  next: any
) {
  if (err.name === "UnauthorizedError") {
    return res.status(401).json({
      description: "Invalid token or expired",
    });
  }
}

// * Authorization by Role Middleware
/**
 *
 * @param formCode
 * form code base on Global Form
 * @param accessCode
 * 1st "Read"
 * 2nd "Create"
 * 3rd "Update"
 * @returns next() or error 401 (Unauthorized)
 */
export function authRoleMiddleware(formCode: string, accessCode = "000") {
  return async (req: Request, res: Response, next: NextFunction) => {
    const userToken: UserToken = decodeToken(String(req.headers.authorization));

    const authCompound = accessCodeConverter(accessCode);

    const authSuperUser = await prisma.tousr.findFirst({
      where: {
        docid: userToken.tousrId,
        gtent_id: {
          docid: userToken.gtentId,
        },
        superuser: 1,
      },
    });

    if (authSuperUser !== null) {
      return next();
    } else if (authCompound !== null) {
      const authAccess = await prisma.taut1.findFirst({
        where: {
          torol_id: {
            docid: userToken.torolId,
          },
          tform_id: {
            formcode: formCode,
          },
          authorization: 1,
          ...authCompound,
        },
      });

      if (authAccess === null) {
        return res.status(401).json({
          description: "User privilage not Fullfield",
        });
      } else {
        return next();
      }
    } else {
      return res.status(401).json({
        description: "User cannot access this module",
      });
    }
  };
}
