import { NextFunction, Request, RequestHandler, Response } from "express";

export const requireAuthorization: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.headers.authorization) {
    const resultObject = {
      statuscode: 400,
      data: {
        description: "Please login first",
      },
    };
    return res.status(resultObject.statuscode).json(resultObject.data);
  } else {
    next();
  }
};
