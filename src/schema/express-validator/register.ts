import { body } from "express-validator";

export const registerRule = [
  body("email").isEmail(),
  body(["username", "password"]).isString().isLength({ max: 100 }),
  body("tohem_id").if(body("usertype").contains(0)).isUUID(),
  body("torol_id").isUUID(),
  body(["statusactive", "activated", "superuser"]).isIn([0, 1]),
  body("provider").not().exists(),
  body("usertype").isIn([0, 1]),
  body("trolleyuser")
    .if(body("usertype").contains(0))
    .isString()
    .isLength({ min: 1, max: 20 }),
  body("trolleypass")
    .if(body("usertype").contains(0))
    .isString()
    .isLength({ min: 1, max: 100 }),
];
