import { body, Schema } from "express-validator";

export const LoginAuthSchema: Schema = {
  identifier: {
    in: "body",
    trim: true,
    isString: true,
    errorMessage: "Identifier must be filled",
  },
  password: {
    in: "body",
    trim: true,
    isString: true,
    errorMessage: "Password must be filled",
  },
};

export const loginRule = [
  body("email").isEmail().isLength({ min: 8, max: 100 }),
  body(["username", "password"]).isString().isLength({ min: 8, max: 100 }),
];
