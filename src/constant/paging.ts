export const PROP_OPTIONS: string[] = [
  "P1",
  "P2",
  "P3",
  "P4",
  "P5",
  "P6",
  "P7",
  "P8",
  "P9",
  "P10",
];

export const CAN_READ = "100";
export const CAN_CREATE = "010";
export const CAN_UPDATE = "001";

export const DOCID_TRIGGER_ERROR = "";

export const MAX_PAGING = 50;
export const MAX_PAGING_GOODSTRACKER = 1000;
