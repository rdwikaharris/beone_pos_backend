import { prisma } from "../config/prisma";
import { errorConsole } from "../utils/console";
import { consoleInspect, consoleLog } from "../utils/console";

export const ItemHierarchy = async (
  catCode: string | undefined,
  gtentId: number
) => {
  /*
   * Category extraction:
   * 1. Just check if catCode is not undefined
   * 2. Get Product hierarchy (tphir), take level and maxchar
   * 3. Loop thru product hierarchy master, split the code based on maxchar
   * 4. Find many product hierarchy master
   * 5. Done
   */

  const _codeSplited: string[] = [];
  let _splitCounter = 0;
  let result: any = null;

  const _newCodeSplited: any[] = [];
  let newResult: any = [];

  if (catCode === undefined) {
    return null;
  }

  try {
    const productHierarchy: any = await prisma.tphir.findMany({
      where: {
        gtentId,
      },
      select: {
        description: true,
        level: true,
        maxchar: true,
      },
    });

    if (!productHierarchy) {
      return null;
    }

    for (const hierarchy in productHierarchy) {
      const _splitToCounter: number = (_splitCounter +
        productHierarchy[hierarchy].maxchar) as number;

      // _codeSplited.push(catCode.substring(_splitCounter, _splitToCounter));

      _newCodeSplited.push({
        code: catCode.substring(_splitCounter, _splitToCounter),
        level: productHierarchy[hierarchy].level,
      });

      _splitCounter = _splitToCounter;
    }

    // result = await prisma.phir1.findMany({
    //   where: {
    //     code: {
    //       in: _codeSplited,
    //     },
    //     gtentId,
    //   },
    //   select: {
    //     code: true,
    //     description: true,
    //     tphir_id: {
    //       select: {
    //         level: true,
    //         description: true,
    //       },
    //     },
    //   },
    // });

    for (const key in _newCodeSplited) {
      const evaluate = await prisma.phir1.findFirst({
        where: {
          code: _newCodeSplited[key].code,
          tphir_id: { level: _newCodeSplited[key].level },
        },
        select: {
          code: true,
          description: true,
          tphir_id: {
            select: {
              level: true,
              description: true,
            },
          },
        },
      });

      newResult.push(evaluate);
    }

    return newResult;
  } catch (err) {
    errorConsole(err);
    return null;
  }
};
