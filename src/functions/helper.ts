import { Request } from "express";

import { UserToken } from "../interface";
import { gtentData } from "../utils/docid-id/global_lookup";
import { torolData, tousrData } from "../utils/docid-id/tenant_lookup_1";

export const validateExecuteRequest = (req: Request): string | null => {
  const { password, old_password, token } = req.body;
  if (password == null) {
    return "password is not provided";
  }
  if (old_password == null) {
    return "Old password is not provided";
  }
  if (token == null) {
    return "Token is not provided";
  }
  return null;
};

export function sameArray(list: any[]) {
  return new Set(list).size === 1;
}

export function uniqueArray(list: any[]) {
  return new Set(list).size === list.length;
}

export async function userState(userToken: UserToken) {
  return {
    user: await tousrData(userToken.tousrId, userToken.gtentId),
    role: await torolData(userToken.torolId, userToken.gtentId),
    tenant: await gtentData(userToken.gtentId),
    datetime: new Date(),
  };
}

export function orderHelper(data: any) {
  let _orderQueryComposite = {};

  const orderParser = data ? JSON.parse(data) : undefined;

  // TODO : Make it dynamic paramter
  if (orderParser) {
    switch (orderParser) {
      case orderParser.vendcode:
        {
          _orderQueryComposite = {
            ..._orderQueryComposite,
            toven_id: {
              vendcode: orderParser.vendcode,
            },
          };
        }
        break;
      case orderParser.vendname:
        {
          _orderQueryComposite = {
            ..._orderQueryComposite,
            toven_id: {
              vendname: orderParser.vendname,
            },
          };
        }
        break;
      default:
        {
          _orderQueryComposite = {
            ..._orderQueryComposite,
            ...orderParser,
          };
        }
        break;
    }
  } else {
    _orderQueryComposite = {
      id: "asc",
    };
  }

  return _orderQueryComposite;
}
