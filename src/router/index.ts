import express from "express";
import { validatePage } from "../middleware";
import { requireAuthorization } from "../middleware/checkHeaders";

const router = express.Router();

// * 00 AUTH
import auth from "../controller/0.auth";
router.use("/auth", validatePage, auth);

// * 01 TENANT MASTER
import gtent from "../controller/1.gtent.TenantMaster";
router.use("/gtent", validatePage, requireAuthorization, gtent);

// * 11 COUNTRY
import tocry from "../controller/11.tocry.Country";
router.use("/tocry", validatePage, requireAuthorization, tocry);

// * 12 PROVINCE
import toprv from "../controller/12.toprv.Province";
router.use("/toprv", validatePage, requireAuthorization, toprv);

// * 13 ZIPCODE
import tozcd from "../controller/13.tozcd.ZipCode";
router.use("/tozcd", validatePage, requireAuthorization, tozcd);

// * 14 STORE
import tostr from "../controller/14.tostr.StoreMaster";
router.use("/tostr", validatePage, requireAuthorization, tostr);

// * 15 EMPLOYEE
import tohem from "../controller/15.tohem.Employee";
router.use("/tohem", validatePage, requireAuthorization, tohem);

// * 16 CASH REGISTER
import tocsr from "../controller/16.tocsr.CashRegister";
router.use("/tocsr", validatePage, requireAuthorization, tocsr);

// * 17 USER
import tousr from "../controller/17.tousr.User";
router.use("/tousr", validatePage, requireAuthorization, tousr);

// * 18 USER ROLE
import torol from "../controller/18.torol.UserRoles";
router.use("/torol", validatePage, requireAuthorization, torol);

// * 19 AUTHORIZATION
import toaut from "../controller/19.toaut.Authorization";
router.use("/toaut", validatePage, requireAuthorization, toaut);

// * 20 AUTHORIZATION BY ROLE
import taut1 from "../controller/20.taut1.AuthorizationByRole";
router.use("/taut1", validatePage, requireAuthorization, taut1);

// * 29 PRODUCT CATEGORY
import tocat from "../controller/29.tocat.ProductCategory";
router.use("/tocat", validatePage, requireAuthorization, tocat);

// * 30 PRODUCT HIERARCHY
import tphir from "../controller/30.tphir.ProductHierarchy";
router.use("/tphir", validatePage, requireAuthorization, tphir);

// * 31 PRODUCT HIERARCHY MASTER
import phir1 from "../controller/31.phir1.ProductHierarchyMaster";
router.use("/phir1", validatePage, requireAuthorization, phir1);

// * 32 ITEM MASTER
import toitm from "../controller/32.toitm.ItemMaster";
router.use("/toitm", validatePage, requireAuthorization, toitm);

// * 33 BARCODE ITEM
import tbitm from "../controller/33.tbitm.BarcodeItem";
router.use("/tbitm", validatePage, requireAuthorization, tbitm);

// * 34 ITEM BY STORE
import tsitm from "../controller/34.tsitm.ItemByStore";
router.use("/tsitm", validatePage, requireAuthorization, tsitm);

// * 35 PREFERED VENDOR
import tvitm from "../controller/35.tvitm.PreferedVendor";
router.use("/tvitm", validatePage, requireAuthorization, tvitm);

// * 36 UNIT OF MEASUREMENT
import touom from "../controller/36.touom.UnitOfMeasurement";
router.use("/touom", validatePage, requireAuthorization, touom);

// * 37 ITEM PICTURE
import tpitm from "../controller/37.tpitm.ItemPicture";
router.use("/tpitm", validatePage, requireAuthorization, tpitm);

// * 38 PRICELIST
import topln from "../controller/38.topln.Pricelist";
router.use("/topln", validatePage, requireAuthorization, topln);

// * 39 PRICELIST PERIOD
import tpln1 from "../controller/39.tpln1.PricelistPeriod";
router.use("/tpln1", validatePage, requireAuthorization, tpln1);

// * 40 PRICE BY ITEM
import tpln2 from "../controller/40.tpln2.PriceByItem";
router.use("/tpln2", validatePage, requireAuthorization, tpln2);

// * 45 FORM MODULES
import tform from "../controller/45.tform.FormModules";
router.use("/tform", validatePage, requireAuthorization, tform);

// * 47 CUSTOMER GROUP
import tocrg from "../controller/47.tocrg.CustomerGroup";
router.use("/tocrg", validatePage, requireAuthorization, tocrg);

// * 47 CUSTOMER GROUP
import tocus from "../controller/49.tocus.Customer";
router.use("/tocus", validatePage, requireAuthorization, tocus);

// * 50 CUSTOMER ADDRESS
import tcus1 from "../controller/50.tcus1.CustomerAddress";
router.use("/tcus1", validatePage, requireAuthorization, tcus1);

// * 51 CUSTOMER CONTACT PERSON
import tcus2 from "../controller/51.tcus2.CustomerContactPerson";
router.use("/tcus2", validatePage, requireAuthorization, tcus2);

// * 55 PAYMENT TERM
import toptr from "../controller/55.toptr.PaymentTerm";
router.use("/toptr", validatePage, requireAuthorization, toptr);

// * 56 ASSIGN PRICE MEMBER PER STORE
import tpln3 from "../controller/56.tpln3.AssignPriceMemberPerStore";
router.use("/tpln3", validatePage, requireAuthorization, tpln3);

// * 57 MASTER PAYMENT TYPE
import gopmt from "../controller/57.gopmt.MasterPaymentType";
router.use("/gopmt", validatePage, requireAuthorization, gopmt);

// * 58 PAYMENT TYPE
import topmt from "../controller/58.topmt.PaymentType";
router.use("/topmt", validatePage, requireAuthorization, topmt);

// * 59 MEANS OF PAYMENT
import tpmt1 from "../controller/59.tpmt1.MeansOfPayment";
router.use("/tpmt1", validatePage, requireAuthorization, tpmt1);

// * 60 CREDIT CARD
import tpmt2 from "../controller/60.tpmt2.CreditCard";
router.use("/tpmt2", validatePage, requireAuthorization, tpmt2);

// * 61 HOUSE BANK ACCOUNT
import tobnk from "../controller/61.tobnk.HouseBankAccount";
router.use("/tobnk", validatePage, requireAuthorization, tobnk);

// * 62 BILL OF MATERIAL
import toitt from "../controller/62.toitt.BillOfMaterial";
router.use("/toitt", validatePage, requireAuthorization, toitt);

// * 63 BILL OF MATERIAL LINE ITEM
import titt1 from "../controller/63.titt1.BillOfMaterialLineItem";
router.use("/titt1", validatePage, requireAuthorization, titt1);

// * 64 ITEM REMARK
import tritm from "../controller/64.tritm.ItemRemark";
router.use("/tritm", validatePage, requireAuthorization, tritm);

// * 67 TAX MASTER
import tovat from "../controller/67.tovat.TaxMaster";
router.use("/tovat", validatePage, requireAuthorization, tovat);

// * 68 PROMO BERTINGKAT
import toprp from "../controller/68.toprp.PromoBertingkat";
router.use("/toprp", validatePage, requireAuthorization, toprp);

// * 95 HOLIDAY
import tohld from "../controller/95.tohld.Holiday";
router.use("/tohld", validatePage, requireAuthorization, tohld);

// * 96 HOLIDAY DETAIL
import thld1 from "../controller/96.thld1.HolidayDetail";
router.use("/thld1", validatePage, requireAuthorization, thld1);

// * 97 INVOICE
import toinv from "../controller/97.toinv.Invoice";
router.use("/toinv", validatePage, requireAuthorization, toinv);

// * 100 CREDIT MEMO
import torin from "../controller/100.torin.CreditMemo";
router.use("/torin", validatePage, requireAuthorization, torin);

// * 103 MOP BY STORE
import tpmt3 from "../controller/103-tpmt3.MOPByStore";
router.use("/tpmt3", validatePage, requireAuthorization, tpmt3);

// * 135 CASHIER BALANCE TRANSACTIONS
import tcsr1 from "../controller/135.tcsr1.CashierBalanceTransaction";
router.use("/tcsr1", validatePage, requireAuthorization, tcsr1);

// * 161 ITEM PROPERTY
import tprop from "../controller/161.tprop.ItemProperty";
router.use("/tprop", validatePage, requireAuthorization, tprop);

// * 198 PRICE BY ITEM BARCODE
import tpln4 from "../controller/198.tpln4.PriceByItemBarcode";
router.use("/tpln4", validatePage, requireAuthorization, tpln4);

export default router;
